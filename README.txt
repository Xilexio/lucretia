Lucretia
--------

Build
-----
Lucretia uses standard cabal build.
The Makefile is using cabal to:
* make - install dependencies and build program
* make install - make + install the program
* make clean - clean workspace

Run
---
In order to run Lucretia properly, set environment variable LUCRETIA_PATH to Lucretia library directory.
Example:
* Windows:
set LUCRETIA_PATH=C:\code\lucretia\lib
* Linux:
export LUCRETIA_PATH=/home/lucretia/lib

After the build, Lucretia binary can be found in dist/build/lucretia/lucretia.

Test
----
After the build, Lucretia test binary can be found in dist/build/test-lucretia/test-lucretia.
LUCRETIA_PATH environment variable has to be set first for it to run properly.