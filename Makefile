all: lucretia

.cabal-sandbox:
	cabal sandbox init

lucretia: .cabal-sandbox
	cabal install --only-dependencies
	cabal configure --enable-tests
	cabal build

install: lucretia
	cabal install

clean:
	cabal clean

.PHONY: lucretia install clean
