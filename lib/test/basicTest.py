def f():
    return "1"

def g(n):
    if n == 0:
        return "1"
    else:
        return g(n - 1)    

def h():
    def gg(n):
        if n == 0:
            return 1
        else:
            return gg(n - 1) * 2
    if gg(4) != 16:
        return "0"
    else:
        return "1"
    
test = f() + g(4) + h()