test = ""

class X(__builtins__.object):
    pass

class Y:
    def foo(self):
        return "1"

y = Y()
test = test + y.foo()
    
class P:
    def __init__(self):
        self.test = "1"
        
    def foo(self):
        return self.test
        
class Q(P):
    def __init__(self):
        self.test = "0"

    def foo(self):
        if self.test == "0":
            return "1"
        else:
            return "0"

p = P()
test = test + p.foo()
            
q = Q()
test = test + q.foo()