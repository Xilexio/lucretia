module Language.Lucretia.ASTArbitrary where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import Data.Char (ord, chr)
import qualified Data.Map as Map
import qualified Data.Set as Set
import Language.Python.Common.SrcLocation (SrcSpan (..))
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen

{-
For generated AST to be finite, number of sub-expressions in generated expression has to be smaller than 1 on average.
There are 17 possible node types with uniform probability over them.
2 expr with 1 subexpr, 1 with 2 subexprs, 2 with 3 subexprs and 2 with maxArgs subexprs. k * 0.824 on average with maxArgs = 2.
Total number of nodes n is solution of n = 1 + k * n in natural numbers with infinity. n = (1 - k)^(-1) = 5,67 here.
-}

-- amount of different identifiers (PropIdent and VarIdent)
maxArbitraryIdents = 8
-- maximum size of generated maps
maxMappingSize = 12
-- note: too many maxArgs will give non-zero probability of an infinite tree
maxArgs = 4
maxStringSize = 12
-- smaller length - bigger name collision chance
maxPropNamePostfixLength = 4
-- maximum number of files in generated file exprs
maxFiles = 12

arbitrarySimpleString = vectorOf maxStringSize $ elements ['a'..'z']
arbitraryArgsCount = elements [0..maxArgs]

instance Arbitrary PropIdent where
    arbitrary = elements $ map PropIdent [1..maxArbitraryIdents]

instance Arbitrary VarIdent where
    arbitrary = elements $ map VarIdent [1..maxArbitraryIdents]

instance Arbitrary ObjPropIdent where
    arbitrary = do
        arbitraryVarIdent <- arbitrary
        arbitraryPropIdent <- arbitrary
        return $ ObjPropIdent arbitraryVarIdent arbitraryPropIdent

instance Arbitrary (Map.Map PropIdent PropIdent) where
    arbitrary = do
        size <- elements [0..maxMappingSize]
        keysAndValues <- vectorOf size arbitrary
        return $ Map.fromList keysAndValues

instance Arbitrary (Expr SrcSpan) where
    arbitrary = oneof [
            return $ ConstNone SpanEmpty,
            do
                arbitraryBool <- arbitrary
                return $ ConstBool arbitraryBool SpanEmpty,
            do
                arbitraryInt <- arbitrary
                return $ ConstInt arbitraryInt SpanEmpty,
            do
                arbitraryFloat <- arbitrary
                return $ ConstFloat arbitraryFloat SpanEmpty,
            do
                arbitraryFloat <- arbitrary
                return $ ConstImaginary arbitraryFloat SpanEmpty,
            do
                arbitraryString <- arbitrarySimpleString
                return $ ConstString arbitraryString SpanEmpty,
            do
                arbitraryVarIdent <- arbitrary
                arbitraryExpr1 <- arbitrary
                arbitraryExpr2 <- arbitrary
                return LetIn {
                    varId = VarIdentAnn arbitraryVarIdent SpanEmpty,
                    binding = arbitraryExpr1,
                    body = arbitraryExpr2,
                    exprAnnot = SpanEmpty
                },
            do
                arbitraryVarIdent <- arbitrary
                return Var {
                    varId = VarIdentAnn arbitraryVarIdent SpanEmpty,
                    exprAnnot = SpanEmpty
                },
            return $ New SpanEmpty,
            do
                arbitraryObjProp <- arbitrary
                return Prop {
                    objProp = ObjPropIdentAnn arbitraryObjProp SpanEmpty,
                    exprAnnot = SpanEmpty
                },
            do
                arbitraryObjProp <- arbitrary
                arbitraryExpr1 <- arbitrary
                return AssignProp {
                    objProp = ObjPropIdentAnn arbitraryObjProp SpanEmpty,
                    value = arbitraryExpr1,
                    exprAnnot = SpanEmpty
                },
            do
                arbitraryExpr1 <- arbitrary
                arbitraryExpr2 <- arbitrary
                arbitraryExpr3 <- arbitrary
                return IfElse {
                    cond = arbitraryExpr1,
                    whenTrue = arbitraryExpr2,
                    whenFalse = arbitraryExpr3,
                    exprAnnot = SpanEmpty
                },
            do
                arbitraryPropIdent <- arbitrary
                arbitraryExpr1 <- arbitrary
                arbitraryExpr2 <- arbitrary
                arbitraryExpr3 <- arbitrary
                return IfHasAttr {
                    objExpr = arbitraryExpr1,
                    prop = arbitraryPropIdent,
                    whenTrue = arbitraryExpr2,
                    whenFalse = arbitraryExpr3,
                    exprAnnot = SpanEmpty
                },
            do
                arbitraryOpType <- arbitraryBoundedEnum
                argsCount <- arbitraryArgsCount
                arbitraryExprs <- vectorOf argsCount arbitrary
                return Op {
                    opType = arbitraryOpType,
                    args = arbitraryExprs,
                    exprAnnot = SpanEmpty
                },
            do
                argsCount <- arbitraryArgsCount
                arbitraryVarIdents <- vectorOf argsCount arbitrary
                arbitraryExpr1 <- arbitrary
                return Fun {
                    argVarIds = map (flip VarIdentAnn SpanEmpty) arbitraryVarIdents,
                    body = arbitraryExpr1,
                    exprAnnot = SpanEmpty
                },
            do
                arbitraryExpr1 <- arbitrary
                argsCount <- arbitraryArgsCount
                arbitraryExprs <- vectorOf argsCount arbitrary
                return Appl {
                    fun = arbitraryExpr1,
                    args = arbitraryExprs,
                    exprAnnot = SpanEmpty
                },
            do
                arbitraryString <- arbitrarySimpleString
                return $ Import (arbitraryString ++ ".luc") SpanEmpty
        ]

instance Arbitrary (FileExprWithMetaData SrcSpan) where
    arbitrary = do
        filePath <- vectorOf maxStringSize $ elements ['a'..'z']
        expr <- arbitrary
        let propIds = Set.toList $ usedPropIds expr
        propNamesPostfixes <- vectorOf (length propIds) $ vectorOf maxPropNamePostfixLength $ elements "ab"
        -- prefixing names with subsequent letters to make them unique
        let propNames = snd $ foldl (\acc name -> (fst acc + 1, (chr (ord 'a' + fst acc) : name) : snd acc)) (0, []) propNamesPostfixes
        return FileExprWithMetaData {
            fExpr = expr,
            fPropNames = Map.fromList $ zip propIds propNames,
            fPropIds = Map.fromList $ zip propNames propIds,
            fPath = filePath ++ ".luc"
        }

instance Arbitrary (FileExprsWithMetaData SrcSpan) where
    arbitrary = do
        filesCount <- elements [0..maxFiles]
        fileExprs <- vectorOf filesCount arbitrary
        return $ foldr addFileExpr emptyFileExprsWithMetaData fileExprs
