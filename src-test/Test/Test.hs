module Test.Test (
    runInvariantTests,
    runLucretiaLibTests,
    runPythonTests,
    runUnitTests
) where

import Control.Applicative ((<$>))
import Control.Arrow ((***))
import Language.Lucretia.AST
import Language.Lucretia.ASTArbitrary
import Language.Lucretia.ASTFunctions
import Language.Util.Common
import Control.Monad
import Language.Python.ConvertEnvironment
import Data.Complex
import Data.List (isPrefixOf, isSuffixOf)
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import qualified Data.Set as Set
import Language.Util.ImportFileIO
import Language.Lucretia.Interpreter
import Language.Python.Common.SrcLocation (SrcSpan (..))
import Language.Pretty.AST
import Language.Pretty.Common
import Language.Pretty.RuntimeError
import Language.Python.Converter
import Language.Python.TestInterpreter
import Language.Lucretia.RunEnvironment
import Language.Lucretia.Parser
import System.Directory
import System.FilePath
import System.IO
import Test.HUnit
import Test.Tools


mkVarId varId = VarIdentAnn (VarIdent varId) SpanEmpty
mkVar varId = Var (mkVarId varId) SpanEmpty
mkObjProp varId propId = ObjPropIdentAnn (ObjPropIdent (VarIdent varId) (PropIdent propId)) SpanEmpty
mkPropExpr varId propId = Prop (mkObjProp varId propId) SpanEmpty

testTree =
    LetIn {
        varId = mkVarId 0,
        binding = Prop {
            objProp = mkObjProp 1 3,
            exprAnnot = SpanEmpty
        },
        body = AssignProp {
            objProp = mkObjProp 2 4,
            value = IfElse {
                cond = Prop {
                    objProp = mkObjProp 1 14,
                    exprAnnot = SpanEmpty
                },
                whenTrue = IfHasAttr {
                    objExpr = mkVar 7,
                    prop = PropIdent 8,
                    whenTrue = AssignProp {
                        objProp = mkObjProp 3 11,
                        value = Prop {
                            objProp = mkObjProp 1 10,
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    },
                    whenFalse = ConstNone SpanEmpty,
                    exprAnnot = SpanEmpty
                },
                whenFalse = Appl {
                    fun = Fun {
                        argVarIds = [mkVarId 1],
                        body = Op Not [Prop {
                            objProp = mkObjProp 6 9,
                            exprAnnot = SpanEmpty
                        },
                        Prop {
                            objProp = mkObjProp 6 13,
                            exprAnnot = SpanEmpty
                        }] SpanEmpty,
                        exprAnnot = SpanEmpty
                    },
                    args = [
                            Prop {
                                objProp = mkObjProp 2 5,
                                exprAnnot = SpanEmpty
                            },
                            Prop {
                                objProp = mkObjProp 2 12,
                                exprAnnot = SpanEmpty
                            }
                        ],
                    exprAnnot = SpanEmpty
                },
                exprAnnot = SpanEmpty
            },
            exprAnnot = SpanEmpty
        },
        exprAnnot = SpanEmpty
    }
testTreePropIds = map PropIdent [3, 4, 5, 8, 9, 10, 11, 12, 13, 14]
testTreeExprNodes = 17
testPropIdsTranslation = Map.fromList $ (PropIdent *** PropIdent) <$> [(3, 4), (4, 0), (1000, 2000)]

tests pythonBuiltins =
    TestList [
        TestLabel "Lucretia parser" $ TestList [
            parseLucretiaTestCase "Boolean operator" (Op Not [ConstBool True SpanEmpty] SpanEmpty) "!(True)",
            parseLucretiaTestCase "Arithmetic operator" (Op Add [ConstImaginary 1 SpanEmpty, ConstFloat 2.4 SpanEmpty] SpanEmpty) "+(1j, 2.4)",
            parseLucretiaTestCase "Function definition and application"
                Appl {
                    fun = Fun {
                        argVarIds = [],
                        body = ConstInt 42 SpanEmpty,
                        exprAnnot = SpanEmpty
                    },
                    args = [],
                    exprAnnot = SpanEmpty
                }
                "func(){ 42 }()",
            parseLucretiaTestCase "Parentheses 1" (ConstInt 42 SpanEmpty) "(42)",
            parseLucretiaTestCase "Parentheses 2" (Op Add [ConstInt 1 SpanEmpty, ConstInt 4 SpanEmpty] SpanEmpty) "(+(1, ((((4))))))",
            parseLucretiaTestCase "Line comments" (ConstInt 42 SpanEmpty)
                "-- x\n\
                \42\n\
                \-- x",
            parseLucretiaTestCase "Block comments" (ConstInt 42 SpanEmpty)
                "{- -}42{- -}",
            parseLucretiaTestCase "Block comment commented out by line comment" (ConstInt 42 SpanEmpty)
                "-- {-\n\
                \42\n\
                \-- -}",
            parseLucretiaTestCase "Line comment commented out by block comment" (ConstInt 42 SpanEmpty)
                "{- -- -}42",
            parseLucretiaTestCase "Nested block comment" (ConstInt 42 SpanEmpty)
                "{- {- nested block comment -} -}42{- {- -} -}",
            parseLucretiaTestCase "Import" (Import "42.luc" SpanEmpty)
                "import(\"42.luc\")",
            parseLucretiaTestCase "Semicolons"
                LetIn {
                    varId = mkVarId 0,
                    binding = ConstNone SpanEmpty,
                    body = LetIn {
                            varId = mkVarId 1,
                            binding = LetIn {
                                varId = mkVarId 2,
                                binding = ConstNone SpanEmpty,
                                body = ConstNone SpanEmpty,
                                exprAnnot = SpanEmpty
                            },
                            body = ConstNone SpanEmpty,
                            exprAnnot = SpanEmpty
                        },
                    exprAnnot = SpanEmpty
                }
                "let x = None; let y = None in None; None"
        ],


----------------


        TestLabel "AST helper functions" $ TestList [
            [ConstBool True SpanEmpty, Op Not [ConstBool True SpanEmpty] SpanEmpty] ~=?
                (foldE (flip (:)) [] $ Op Not [ConstBool True SpanEmpty] SpanEmpty),
            testTreeExprNodes ~=? foldE (\acc expr -> acc + 1) 0 testTree,
            Set.empty ~=? importedFilePaths (ConstNone SpanEmpty),
            Set.fromList ["test1.luc", "test2.luc"] ~=? importedFilePaths
                LetIn {
                    varId = mkVarId 42,
                    binding = Import "test1.luc" SpanEmpty,
                    body = Import "test2.luc" SpanEmpty,
                    exprAnnot = SpanEmpty
                },
            Set.fromList testTreePropIds ~=? usedPropIds testTree,
            (substitutePropIds (Map.fromList [(PropIdent 2, PropIdent 3)]) $
                Op Mul [Prop (mkObjProp 1 2) SpanEmpty, Prop (mkObjProp 2 4) SpanEmpty] SpanEmpty) ~?=
                Op Mul [Prop (mkObjProp 1 3) SpanEmpty, Prop (mkObjProp 2 4) SpanEmpty] SpanEmpty,
            FileExprsWithMetaData {
                    fExprs = Map.singleton "source.luc" $ ConstNone SpanEmpty,
                    fAllPropNames = Map.empty,
                    fAllPropIds = Map.empty
                } ~=? addFileExpr FileExprWithMetaData {
                    fExpr = ConstNone SpanEmpty,
                    fPropNames = Map.empty,
                    fPropIds = Map.empty,
                    fPath = "source.luc"
                } emptyFileExprsWithMetaData,
            FileExprsWithMetaData {
                    fExprs = Map.fromList [("f1.luc", mkPropExpr 1 2), ("f2.luc", mkPropExpr 2 3)],
                    fAllPropNames = Map.fromList [(PropIdent 2, "foo"), (PropIdent 3, "bar")],
                    fAllPropIds = Map.fromList [("foo", PropIdent 2), ("bar", PropIdent 3)]
                } ~=? addFileExpr FileExprWithMetaData {
                    fExpr = mkPropExpr 2 3,
                    fPropNames = Map.singleton (PropIdent 3) "bar",
                    fPropIds = Map.singleton "bar" (PropIdent 3),
                    fPath = "f2.luc"
                } FileExprsWithMetaData {
                    fExprs = Map.singleton "f1.luc" $ mkPropExpr 1 2,
                    fAllPropNames = Map.singleton (PropIdent 2) "foo",
                    fAllPropIds = Map.singleton "foo" (PropIdent 2)
                },
            FileExprsWithMetaData {
                    fExprs = Map.fromList [("f1.luc", mkPropExpr 1 42), ("f2.luc", mkPropExpr 2 42)],
                    fAllPropNames = Map.fromList [(PropIdent 42, "same")],
                    fAllPropIds = Map.fromList [("same", PropIdent 42)]
                } ~=? addFileExpr FileExprWithMetaData {
                    fExpr = mkPropExpr 2 44,
                    fPropNames = Map.singleton (PropIdent 44) "same",
                    fPropIds = Map.singleton "same" (PropIdent 44),
                    fPath = "f2.luc"
                } FileExprsWithMetaData {
                    fExprs = Map.singleton "f1.luc" $ mkPropExpr 1 42,
                    fAllPropNames = Map.singleton (PropIdent 42) "same",
                    fAllPropIds = Map.singleton "same" (PropIdent 42)
                }
        ],


----------------


        TestLabel "Lucretia interpreter" $ TestList [
            TestLabel "Consts" $ TestList [
                evalPositiveTestCase "Integer evaluation" (IntVal 42) $ ConstInt 42 SpanEmpty,
                evalPositiveTestCase "Bool evaluation" (BoolVal True) $ ConstBool True SpanEmpty,
                evalPositiveTestCase "Float evaluation" (FloatVal 42) $ ConstFloat 42 SpanEmpty,
                evalPositiveTestCase "Imaginary value evaluation" (ComplexVal $ 0 :+ 42) $ ConstImaginary 42 SpanEmpty,
                evalPositiveTestCase "String evaluation" (StringVal "42") $ ConstString "42" SpanEmpty,
                evalPositiveTestCase "None value evaluation" NoneVal $ ConstNone SpanEmpty
            ],


            TestLabel "Operators" $ TestList [
                evalPositiveTestCase "Integer operations" (IntVal 42) $
                    Op Add [
                        Op Mod [
                            Op Sub [ConstInt (-42) SpanEmpty] SpanEmpty,
                            Op Sub [ConstInt 12 SpanEmpty, ConstInt 8 SpanEmpty] SpanEmpty
                        ] SpanEmpty,
                        Op Mul [ConstInt (-2) SpanEmpty, ConstInt (-20) SpanEmpty] SpanEmpty
                    ] SpanEmpty,
                evalPositiveTestCase "Not" (BoolVal True) $ Op Not [ConstBool False SpanEmpty] SpanEmpty,
                evalPositiveTestCase "Float modulo - positive values" (FloatVal 0.25) $
                    Op Mod [ConstFloat 4 SpanEmpty, ConstFloat 0.75 SpanEmpty] SpanEmpty,
                evalPositiveTestCase "Float modulo - negative values" (FloatVal (-0.5)) $
                    Op Mod [ConstFloat (-3) SpanEmpty, ConstFloat (-1.25) SpanEmpty] SpanEmpty,
                evalPositiveTestCase "Float modulo - first positive, second negative" (FloatVal (-0.75)) $
                    Op Mod [ConstFloat 3 SpanEmpty, ConstFloat (-1.25) SpanEmpty] SpanEmpty,
                evalPositiveTestCase "Float modulo - first negative, second positive" (FloatVal 1) $
                    Op Mod [ConstFloat (-2) SpanEmpty, ConstFloat 1.5 SpanEmpty] SpanEmpty,
                evalPositiveTestCase "Float operations" (FloatVal 42) $
                    Op Add [
                        Op Mod [
                            Op Sub [ConstFloat (-42) SpanEmpty] SpanEmpty,
                            Op Sub [ConstFloat 12 SpanEmpty, ConstFloat 8 SpanEmpty] SpanEmpty
                        ] SpanEmpty,
                        Op Mul [
                            ConstFloat (-2) SpanEmpty,
                            Op Div [ConstFloat (-60) SpanEmpty, ConstFloat 3 SpanEmpty] SpanEmpty
                        ] SpanEmpty
                    ] SpanEmpty,
                evalPositiveTestCase "Complex number evaluation" (ComplexVal $ 42 :+ 42) $
                    Op Add [ConstFloat 42 SpanEmpty, ConstImaginary 42 SpanEmpty] SpanEmpty,
                evalPositiveTestCase "Complex number operations 1" (ComplexVal $ 42 :+ 42) $
                    Op Add [
                        Op Add [ConstFloat 22 SpanEmpty, ConstImaginary 10 SpanEmpty] SpanEmpty,
                        Op Sub [
                            Op Add [ConstFloat 20 SpanEmpty, ConstImaginary 21 SpanEmpty] SpanEmpty,
                            ConstImaginary (-11) SpanEmpty
                        ] SpanEmpty
                    ] SpanEmpty,
                evalPositiveTestCase "Complex number operations 2" (ComplexVal $ 42 :+ 42) $
                    Op Add [
                        Op Sub [
                            Op Sub [
                                Op Add [ConstFloat 12 SpanEmpty, ConstImaginary (-128) SpanEmpty] SpanEmpty,
                                Op Add [ConstFloat 96 SpanEmpty, ConstImaginary (-44) SpanEmpty] SpanEmpty
                            ] SpanEmpty
                        ] SpanEmpty,
                        Op Mul [
                            ConstImaginary 42 SpanEmpty,
                            Op Div [
                                ConstFloat (-2) SpanEmpty,
                                Op Add [ConstFloat 1 SpanEmpty, ConstImaginary 1 SpanEmpty] SpanEmpty
                            ] SpanEmpty
                        ] SpanEmpty
                    ] SpanEmpty,
                evalPositiveTestCase "Comparison" (BoolVal True) $
                    Op BitAnd [
                        Op BitAnd [
                            Op Lesser [ConstFloat 1 SpanEmpty, ConstFloat 2 SpanEmpty] SpanEmpty,
                            Op LesserOrEq [ConstInt 1 SpanEmpty, ConstInt 1 SpanEmpty] SpanEmpty
                        ] SpanEmpty,
                        Op BitAnd [
                            Op Greater [ConstString "bc" SpanEmpty, ConstString "bb" SpanEmpty] SpanEmpty,
                            Op BitAnd [
                                Op GreaterOrEq [ConstFloat 44 SpanEmpty, ConstInt 42 SpanEmpty] SpanEmpty,
                                Op Lesser [ConstInt 42 SpanEmpty, ConstFloat 44 SpanEmpty] SpanEmpty
                            ] SpanEmpty
                        ] SpanEmpty
                    ] SpanEmpty,
                evalPositiveTestCase "Equality" (BoolVal False) $
                    Op BitOr [
                        Op BitOr [
                            Op Equal [ConstString "x" SpanEmpty, ConstString "y" SpanEmpty] SpanEmpty,
                            Op NotEqual [ConstInt 42 SpanEmpty, ConstFloat 42 SpanEmpty] SpanEmpty
                        ] SpanEmpty,
                        Op BitOr [
                            Op BitOr [
                                Op BitOr [
                                    Op NotEqual [ConstImaginary 2 SpanEmpty, ConstImaginary 2 SpanEmpty] SpanEmpty,
                                    Op Equal [New SpanEmpty, New SpanEmpty] SpanEmpty
                                ] SpanEmpty,
                                Op BitOr [
                                    Op Equal [ConstBool True SpanEmpty, ConstBool False SpanEmpty] SpanEmpty,
                                    Op NotEqual [
                                        Op Add [ConstImaginary 42 SpanEmpty, ConstFloat 42 SpanEmpty] SpanEmpty,
                                        Op Sub [ConstImaginary 42 SpanEmpty, ConstFloat (-42) SpanEmpty] SpanEmpty
                                    ] SpanEmpty
                                ] SpanEmpty
                            ] SpanEmpty,
                            Op BitOr [
                                Op NotEqual [ConstNone SpanEmpty, ConstNone SpanEmpty] SpanEmpty,
                                Op NotEqual [
                                    ConstInt 42 SpanEmpty,
                                    Op Add [ConstInt 40 SpanEmpty, ConstInt 2 SpanEmpty] SpanEmpty
                                ] SpanEmpty
                            ] SpanEmpty
                        ] SpanEmpty
                    ] SpanEmpty,
                evalPositiveTestCase "Same" (BoolVal False) $
                    Op BitOr [
                        Op Equal [New SpanEmpty, New SpanEmpty] SpanEmpty,
                        Op NotEqual [ConstNone SpanEmpty, ConstNone SpanEmpty] SpanEmpty
                    ] SpanEmpty
            ],


            TestLabel "let in" $ TestList [
                evalPositiveTestCase "Basic let in" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = ConstInt 42 SpanEmpty,
                        body = mkVar 0,
                        exprAnnot = SpanEmpty
                    },
                evalPositiveTestCase "Nested let in" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = LetIn {
                            varId = mkVarId 1,
                            binding = ConstInt 42 SpanEmpty,
                            body = mkVar 1,
                            exprAnnot = SpanEmpty
                        },
                        body = LetIn {
                            varId = mkVarId 2,
                            binding = mkVar 0,
                            body = mkVar 2,
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    },
                evalNegativeTestCase "Undefined variable" $ mkVar 0,
                evalNegativeTestCase "Forced evaluation of binding"
                    LetIn {
                        varId = mkVarId 0,
                        binding = Op Div [ConstInt 0 SpanEmpty, ConstInt 0 SpanEmpty] SpanEmpty,
                        body = ConstNone SpanEmpty,
                        exprAnnot = SpanEmpty
                    },
                evalNegativeTestCase "Variable out of scope"
                    LetIn {
                        varId = mkVarId 0,
                        binding = LetIn {
                            varId = mkVarId 1,
                            binding = ConstInt 42 SpanEmpty,
                            body = ConstNone SpanEmpty,
                            exprAnnot = SpanEmpty
                        },
                        body = mkVar 1,
                        exprAnnot = SpanEmpty
                    }
            ],


            TestLabel "if" $ TestList [
                evalPositiveTestCase "if evaluation" (IntVal 42)
                    IfElse {
                        cond = ConstBool False SpanEmpty,
                        whenTrue = ConstFloat 44 SpanEmpty,
                        whenFalse = ConstInt 42 SpanEmpty,
                        exprAnnot = SpanEmpty
                    },
                evalPositiveTestCase "Short circuit if" (IntVal 42)
                    IfElse {
                        cond = ConstBool True SpanEmpty,
                        whenTrue = ConstInt 42 SpanEmpty,
                        whenFalse = Op Div [ConstInt 0 SpanEmpty, ConstInt 0 SpanEmpty] SpanEmpty,
                        exprAnnot = SpanEmpty
                    },
                evalPositiveTestCase "Nested ifs with short circuit" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = ConstBool True SpanEmpty,
                        body = IfElse {
                            cond = Op Not [mkVar 0] SpanEmpty,
                            whenTrue = Op Div [ConstInt 0 SpanEmpty, ConstInt 0 SpanEmpty] SpanEmpty,
                            whenFalse = IfElse {
                                cond = mkVar 0,
                                whenTrue = ConstInt 42 SpanEmpty,
                                whenFalse = Op Div [ConstInt 0 SpanEmpty, ConstInt 0 SpanEmpty] SpanEmpty,
                                exprAnnot = SpanEmpty
                            },
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    }
            ],


            TestLabel "Objects" $ TestList [
                evalPositiveTestCase "Basic property assignment and reading" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = New SpanEmpty,
                        body = LetIn {
                            varId = mkVarId 1,
                            binding = AssignProp {
                                objProp = mkObjProp 0 2,
                                value = ConstInt 42 SpanEmpty,
                                exprAnnot = SpanEmpty
                            },
                            body = Prop {
                                objProp = mkObjProp 0 2,
                                exprAnnot = SpanEmpty
                            },
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    },
                evalPositiveTestCase "Properties with nested let in" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = New SpanEmpty,
                        body = LetIn {
                            varId = mkVarId 1,
                            binding = LetIn {
                                varId = mkVarId 3,
                                binding = AssignProp {
                                    objProp = mkObjProp 0 2,
                                    value = ConstInt 42 SpanEmpty,
                                    exprAnnot = SpanEmpty
                                },
                                body = ConstNone SpanEmpty,
                                exprAnnot = SpanEmpty
                            },
                            body = LetIn {
                                varId = mkVarId 4,
                                binding = LetIn {
                                    varId = mkVarId 5,
                                    binding = ConstNone SpanEmpty,
                                    body = AssignProp {
                                        objProp = mkObjProp 0 4,
                                        value = Prop {
                                            objProp = mkObjProp 0 2,
                                            exprAnnot = SpanEmpty
                                        },
                                        exprAnnot = SpanEmpty
                                    },
                                    exprAnnot = SpanEmpty
                                },
                                body = Prop {
                                    objProp = mkObjProp 0 4,
                                    exprAnnot = SpanEmpty
                                },
                                exprAnnot = SpanEmpty
                            },
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    },
                evalPositiveTestCase "Multiple properties of global object" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = New SpanEmpty,
                        body = LetIn {
                            varId = mkVarId 1,
                            binding = LetIn {
                                varId = mkVarId 2,
                                binding = ConstInt 42 SpanEmpty,
                                body = LetIn {
                                    varId = mkVarId 3,
                                    binding = AssignProp {
                                        objProp = mkObjProp 0 4,
                                        value = mkVar 2,
                                        exprAnnot = SpanEmpty
                                    },
                                    body = ConstNone SpanEmpty,
                                    exprAnnot = SpanEmpty
                                },
                                exprAnnot = SpanEmpty
                            },
                            body = LetIn {
                                varId = mkVarId 5,
                                binding = LetIn {
                                    varId = mkVarId 6,
                                    binding = AssignProp {
                                        objProp = mkObjProp 0 7,
                                        value = Prop {
                                            objProp = mkObjProp 0 4,
                                            exprAnnot = SpanEmpty
                                        },
                                        exprAnnot = SpanEmpty
                                    },
                                    body = ConstNone SpanEmpty,
                                    exprAnnot = SpanEmpty
                                },
                                body = LetIn {
                                    varId = mkVarId 8,
                                    binding = LetIn {
                                        varId = mkVarId 9,
                                        binding = ConstString "x" SpanEmpty,
                                        body = LetIn {
                                            varId = mkVarId 10,
                                            binding = AssignProp {
                                                objProp = mkObjProp 0 11,
                                                value = mkVar 9,
                                                exprAnnot = SpanEmpty
                                            },
                                            body = ConstNone SpanEmpty,
                                            exprAnnot = SpanEmpty
                                        },
                                        exprAnnot = SpanEmpty
                                    },
                                    body = Prop {
                                        objProp = mkObjProp 0 7,
                                        exprAnnot = SpanEmpty
                                    },
                                    exprAnnot = SpanEmpty
                                },
                                exprAnnot = SpanEmpty
                            },
                            exprAnnot = SpanEmpty
                       },
                       exprAnnot = SpanEmpty
                    },
                evalNegativeTestCase "Undefined object"
                    Prop {
                        objProp = mkObjProp 0 1,
                        exprAnnot = SpanEmpty
                    },
                evalNegativeTestCase "Undefined property"
                    LetIn {
                        varId = mkVarId 0,
                        binding = New SpanEmpty,
                        body = Prop {
                            objProp = mkObjProp 0 1,
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    }
            ],


            TestLabel "Basic functions" $ TestList [
                evalPositiveTestCase "Definition and application of 0-arg function" (IntVal 42)
                    Appl {
                        fun = Fun {
                            argVarIds = [],
                            body = ConstInt 42 SpanEmpty,
                            exprAnnot = SpanEmpty
                        },
                        args = [],
                        exprAnnot = SpanEmpty
                    },
                evalPositiveTestCase "Definition and application of 2-arg function" (IntVal 42)
                    Appl {
                        fun = Fun {
                            argVarIds = [mkVarId 0, mkVarId 1],
                            body = Op Sub [mkVar 0, mkVar 1] SpanEmpty,
                            exprAnnot = SpanEmpty
                        },
                        args = [ConstInt 40 SpanEmpty, ConstInt (-2) SpanEmpty],
                        exprAnnot = SpanEmpty
                    },
                evalNegativeTestCase "Evaluation of all arguments"
                    Appl {
                        fun = Fun {
                            argVarIds = [mkVarId 0],
                            body = ConstInt 42 SpanEmpty,
                            exprAnnot = SpanEmpty
                        },
                        args = [Op Div [ConstInt 0 SpanEmpty, ConstInt 0 SpanEmpty] SpanEmpty],
                        exprAnnot = SpanEmpty
                    },
                evalPositiveTestCase "Object method" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = New SpanEmpty,
                        body = LetIn {
                            varId = mkVarId 1,
                            binding = AssignProp {
                                objProp = mkObjProp 0 2,
                                value = ConstInt 42 SpanEmpty,
                                exprAnnot = SpanEmpty
                            },
                            body = LetIn {
                                varId = mkVarId 3,
                                binding = AssignProp {
                                    objProp = mkObjProp 0 4,
                                    value = Fun {
                                        argVarIds = [mkVarId 5],
                                        body = Prop {
                                            objProp = mkObjProp 5 2,
                                            exprAnnot = SpanEmpty
                                        },
                                        exprAnnot = SpanEmpty
                                    },
                                    exprAnnot = SpanEmpty
                                },
                                body = Appl {
                                    fun = Prop {
                                        objProp = mkObjProp 0 4,
                                        exprAnnot = SpanEmpty
                                    },
                                    args = [mkVar 0],
                                    exprAnnot = SpanEmpty
                                },
                                exprAnnot = SpanEmpty
                            },
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    }
            ],


            TestLabel "Functions with closure" $ TestList [
                evalPositiveTestCase "Basic closure" (IntVal 42)
                    LetIn {
                        varId = mkVarId 0,
                        binding = LetIn {
                            varId = mkVarId 1,
                            binding = ConstInt 42 SpanEmpty,
                            body = Fun {
                                argVarIds = [],
                                body = mkVar 1,
                                exprAnnot = SpanEmpty
                            },
                            exprAnnot = SpanEmpty
                        },
                        body = Appl {
                            fun = mkVar 0,
                            args = [],
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    }
            ],


            TestLabel "Recursive functions" $ TestList [
                evalPositiveTestCase "Fibonacci" (IntVal 21)
                    LetIn {
                        varId = mkVarId 0,
                        binding = New SpanEmpty,
                        body = LetIn {
                            varId = mkVarId 1,
                            binding = AssignProp {
                                objProp = mkObjProp 0 2,
                                value = Fun {
                                    argVarIds = [mkVarId 3],
                                    body = IfElse {
                                        cond = Op LesserOrEq [mkVar 3, ConstInt 2 SpanEmpty] SpanEmpty,
                                        whenTrue = ConstInt 1 SpanEmpty,
                                        whenFalse = Op Add [
                                            Appl {
                                                fun = Prop {
                                                    objProp = mkObjProp 0 2,
                                                    exprAnnot = SpanEmpty
                                                },
                                                args = [Op Sub [mkVar 3, ConstInt 1 SpanEmpty] SpanEmpty],
                                                exprAnnot = SpanEmpty
                                            },
                                            Appl {
                                                fun = Prop {
                                                    objProp = mkObjProp 0 2,
                                                    exprAnnot = SpanEmpty
                                                },
                                                args = [Op Sub [mkVar 3, ConstInt 2 SpanEmpty] SpanEmpty],
                                                exprAnnot = SpanEmpty
                                            }
                                        ] SpanEmpty,
                                        exprAnnot = SpanEmpty
                                    },
                                    exprAnnot = SpanEmpty
                                },
                                exprAnnot = SpanEmpty
                            },
                            body = Appl {
                                fun = Prop {
                                    objProp = mkObjProp 0 2,
                                    exprAnnot = SpanEmpty
                                },
                                args = [ConstInt 8 SpanEmpty],
                                exprAnnot = SpanEmpty
                            },
                            exprAnnot = SpanEmpty
                        },
                        exprAnnot = SpanEmpty
                    }
            ],


            TestLabel "Resuming" $ TestList [
                resumeEvalPositiveTestCase "Single resume" (replicate 2 $ IntVal 42)
                    [
                        LetSegment {
                            iVarId = mkVarId 0,
                            iBinding = ConstInt 42 SpanEmpty
                        },
                        LucretiaExpr $ mkVar 0
                    ],
                resumeEvalPositiveTestCase "Triple resume" [IntVal 42, IntVal 42, IntVal 44, IntVal 44]
                    [
                        LetSegment {
                            iVarId = mkVarId 0,
                            iBinding = ConstInt 42 SpanEmpty
                        },
                        LucretiaExpr $ mkVar 0,
                        LetSegment {
                            iVarId = mkVarId 1,
                            iBinding = ConstInt 44 SpanEmpty
                        },
                        LucretiaExpr $ mkVar 1
                    ]
            ]
        ],


----------------


        TestLabel "Lucretia parser + interpreter" $ TestList [
            TestLabel "Literals" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "Integer interpretation" (IntVal 42) "42",
                parseAndEvalLucretiaPositiveTestCase "Bool interpretation" (BoolVal True) "True",
                parseAndEvalLucretiaPositiveTestCase "Float interpretation" (FloatVal 42) "42.0",
                parseAndEvalLucretiaPositiveTestCase "Imaginary number interpretation" (ComplexVal $ 0 :+ 42) "42j",
                parseAndEvalLucretiaPositiveTestCase "String interpretation" (StringVal "42") "\"42\"",
                parseAndEvalLucretiaPositiveTestCase "None value interpretation" NoneVal "None"
            ],


            TestLabel "Operators" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "Basic arithmetic with different types" (ComplexVal $ 42 :+ 42)
                    "*(-(*(2j, +(1j, -(1)))), -(**(3, 3), 6.0))",
                parseAndEvalLucretiaPositiveTestCase "Comparisons" (BoolVal True)
                    "&(<(2, 3.0), >=(True, ==(1, 0)))",
                parseAndEvalLucretiaPositiveTestCase "Equality and is for None" (BoolVal True)
                    "&( \
                    \  &(==(None, None), ===(None, None)), \
                    \  !(|(!=(None, None), !==(None, None))) \
                    \)"
            ],


            TestLabel "Objects" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "Basic property interpretation" (IntVal 42)
                    "let x = new in \
                    \let _ = x.a = 42 in \
                    \x.a",
                parseAndEvalLucretiaPositiveTestCase "Nested property interpretation" (IntVal 42)
                    "let x = new in \
                    \let _ = x.a = new in \
                    \let a = x.a in \
                    \let _ = a.b = 42 in \
                    \let q = x.a in \
                    \q.b",
                parseAndEvalLucretiaPositiveTestCase "Value returned by assignment and order of execution" (IntVal 42)
                    "let x = new in \
                    \x.z = x.y = x.x = 42"
            ],


            TestLabel "Conversions" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "False equals 0" (BoolVal True) "==(False, 0)",
                parseAndEvalLucretiaPositiveTestCase "False is not 0" (BoolVal False) "===(False, 0)"
            ],


            TestLabel "ifhasattr" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "Basic ifhasattr - literal" (IntVal 0)
                    "ifhasattr(0, x) then 1 else 0",
                parseAndEvalLucretiaPositiveTestCase "Basic ifhasattr - negative case" (BoolVal False)
                    "ifhasattr(new, x) then True else False",
                parseAndEvalLucretiaPositiveTestCase "Basic ifhasattr - positive case" (BoolVal True)
                    "let a = new in \
                    \let _ = a.x = 42 in \
                    \ifhasattr(a, x) then True else False"
            ],


            TestLabel "Scopes and idents" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "Same variable name" (IntVal 42)
                    "let x = 0 in let x = let x = 0 in 42 in x",
                parseAndEvalLucretiaPositiveTestCase "Variable and property with the same name" (IntVal 42)
                    "let x = new in let _ = x.x = 42 in x.x",
                parseAndEvalLucretiaPositiveTestCase "Non-recursive redefinition of a variable" (IntVal 42)
                    "let x = 42 in let x = x in x"
            ],


            TestLabel "Functions" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "Function without arguments" (IntVal 42)
                    "func(){ 42 }()",
                parseAndEvalLucretiaPositiveTestCase "Function with one argument" (IntVal 42)
                    "func(x){ x }(42)",
                parseAndEvalLucretiaPositiveTestCase "Function with two arguments" (IntVal 42)
                    "func(x,y){ //(x, y) }(84, 2)",
                parseAndEvalLucretiaNegativeTestCase "Evaluation of every argument"
                    "func(x){ 42 }(/(1, 0))",
                parseAndEvalLucretiaPositiveTestCase "Argument evaluation order" (IntVal 42)
                    "let n = new in \
                    \func(x, y){ n.n }( \
                    \  n.n = 44, n.n = 42 \
                    \)",
                parseAndEvalLucretiaPositiveTestCase "Non-recursive let" (IntVal 42)
                    "let f = func(){ 42 } in \
                    \let f = func(){ f() } in \
                    \f()"
            ],


            TestLabel "Syntax sugar" $ TestList [
                parseAndEvalLucretiaPositiveTestCase "Semicolon - lets" (IntVal 42)
                    "let x = 22; \
                    \let y = 20; \
                    \+(x, y)",
                parseAndEvalLucretiaPositiveTestCase "Semicolon - sequence and let" (IntVal 42)
                    "let x = new; \
                    \x.a = 42; \
                    \x.a",
                parseAndEvalLucretiaPositiveTestCase "Multiple dots" (IntVal 42)
                    "let x = new in \
                    \let _ = x.a = new in \
                    \let _ = x.a.b = 42 in \
                    \x.a.b"
            ]
        ],


----------------


        TestLabel "Python converter + Lucretia interpreter" $ TestList [
            TestLabel "Literals" $ TestList [
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Integer interpretation" (IntVal 42) "test = 42",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Bool interpretation" (BoolVal True) "test = True",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Float interpretation" (FloatVal 42) "test = 42.0",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Imaginary number interpretation" (ComplexVal $ 0 :+ 42) "test = 42j",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "String interpretation" (StringVal "42") "test = \"42\"",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Long string interpretation" (StringVal "42") "test = '''42'''",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Concatenated string interpretation" (StringVal "42") "test = '4' \"2\"",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Concatenated long string interpretation" (StringVal "42") "test = \"\"\"4\"\"\" '''2'''",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "String with escaped newline interpretation" (StringVal "a\nb") "test = 'a\\nb'",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "String with raw modifier interpretation 1" (StringVal "a\\nb") "test = r'a\\nb'",
                -- disabled because of a bug in Language.Python.Common.StringEscape
                -- parseAndEvalPython3PositiveTestCase pythonBuiltins "String with raw modifier interpretation 2" (StringVal "\\\"") "test = r\"\\\"\"",
                -- Python 3 supports unicode strings by default, but is compatibile with Python 2 notation
                -- disabled because of lack of support in Language.Python.Version3
                -- parseAndEvalPython3PositiveTestCase pythonBuiltins "Unicode string" (StringVal "42") "test = u\"42\"",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "None value interpretation" NoneVal "test = None"
            ],


            parseAndEvalPython3PositiveTestCase pythonBuiltins "pass" (IntVal 42)
                "test = 42\n\
                \pass\n\
                \pass",


            TestLabel "Operators" $ TestList [
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Integer arithmetic" (IntVal 42)
                    "test = -2 * -(41 // 2) + 3**4**5 % 7 - 2",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Float arithmetic" (FloatVal 42)
                    "test = -2.0 * -(41.12 // 2.0) + 1.0**1.3**4.3 - 2.25 % 1.25 + 9 / 3 - 2.0 / 2.0",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Bool arithmetic and comparisons" (BoolVal True)
                    "test = (42 == 42.0) and (True != False) and (42 > True) and (False <= True) or not True",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Bool bit arithmetic" (BoolVal False)
                    "test = True ^ True or False | False or True & False",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Bitwise inversion" (IntVal 42)
                    "test = ~~42",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Integer bit arithmetic" (IntVal 2)
                    "test = ~(-1) | (3 & 6)"
            ],


            TestLabel "Functions" $ TestList [
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Definition and evaluation of function without arguments" (IntVal 42)
                    "def foo():\n\
                    \  return 42\n\
                    \test = foo()",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Definition and evaluation of function with one argument" (IntVal 42)
                    "def foo(x):\n\
                    \  return x\n\
                    \test = foo(42)",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Definition and evaluation of function with two arguments" (IntVal 42)
                    "def foo(x, y):\n\
                    \  return x // y\n\
                    \test = foo(84, 2)",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Recursive function" (IntVal 24)
                    "def factorial(n):\n\
                    \  if n < 2:\n\
                    \    return 1\n\
                    \  else:\n\
                    \    return n * factorial(n-1)\n\
                    \test = factorial(4)"
            ],


            TestLabel "while" $ TestList [
                parseAndEvalPython3PositiveTestCase pythonBuiltins "while False" (IntVal 42)
                    "test = 42\n\
                    \while False: pass",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Basic while 1" (IntVal 42)
                    "test = 0\n\
                    \while test < 42:\n\
                    \  test = test + 1",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Basic while 2" (IntVal $ 6^8)
                    "test = 6\n\
                    \while test % 256 != 0:\n\
                    \  test = test * test",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "while with break" (IntVal 42)
                    "test = 42\n\
                    \while test != 0:\n\
                    \  break\n\
                    \  test = 0",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "while with continue" (IntVal 42)
                    "test = 0\n\
                    \while test != 42:\n\
                    \  test = 42\n\
                    \  continue\n\
                    \  test = 0",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "while with return" (IntVal 42)
                    "def foo():\n\
                    \  while True:\n\
                    \    return 42\n\
                    \test = foo()",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "while with else" (IntVal 42)
                    "while False:\n\
                    \  pass\n\
                    \else:\n\
                    \  test = 42",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "while with else and break" (IntVal 42)
                    "test = 42\n\
                    \while True:\n\
                    \  break\n\
                    \else:\n\
                    \  test = 0",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Basic nested while" (IntVal 42)
                    "test = 1\n\
                    \while test % 7 != 0:\n\
                    \  test = test + 1\n\
                    \  while test % 6 != 0:\n\
                    \    test = test + 1",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Nested while with break" (IntVal 42)
                    "test = 0\n\
                    \while True:\n\
                    \  while True:\n\
                    \    test = 44\n\
                    \    break\n\
                    \  test = 42\n\
                    \  break",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "return before break" (IntVal 42)
                    "def foo():\n\
                    \  while True:\n\
                    \    return 42\n\
                    \    break\n\
                    \  return 0\n\
                    \test = foo()",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "break before return" (IntVal 42)
                    "def foo():\n\
                    \  while True:\n\
                    \    break\n\
                    \    return 44\n\
                    \  return 42\n\
                    \test = foo()",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Single break in nested while" (IntVal 42)
                    "test = 0\n\
                    \x = 0\n\
                    \while x != 21:\n\
                    \  while True:\n\
                    \    test = test + 2\n\
                    \    break\n\
                    \  x = x + 1"
            ],


            TestLabel "class" $ TestList [
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Dummy class" NoneVal
                    "class X:\n\
                    \  pass\n\
                    \test = None",
                parseAndEvalPython3PositiveTestCase pythonBuiltins "Class with a single static property" (IntVal 42)
                    "class X:\n\
                    \  a = 42\n\
                    \test = X.a"
            ]
        ]
    ]


prop_substitutePropIds :: (ExprSpan, Map.Map PropIdent PropIdent) -> Bool
prop_substitutePropIds (expr, conv) =
    Set.map (\x -> Map.findWithDefault x x conv) (usedPropIds expr) == usedPropIds (substitutePropIds conv expr)

prop_addFileExprToEmpty :: FileExprWithMetaDataSpan -> Bool
prop_addFileExprToEmpty fileExpr =
    getFileExpr (fPath fileExpr) (addFileExpr fileExpr emptyFileExprsWithMetaData) == Just fileExpr

prop_usedFileExprsPropIds :: FileExprsWithMetaDataSpan -> Bool
prop_usedFileExprsPropIds fileExprs =
    Map.foldr (Set.union . usedPropIds) Set.empty (fExprs fileExprs) `Set.isSubsetOf` Map.keysSet (fAllPropNames fileExprs)

prop_propReverseMappingInFileExpr :: FileExprWithMetaDataSpan -> Bool
prop_propReverseMappingInFileExpr fileExpr =
    fPropIds fileExpr == reverseMapping (fPropNames fileExpr)

prop_propReverseMappingInFileExprs :: FileExprsWithMetaDataSpan -> Bool
prop_propReverseMappingInFileExprs fileExprs =
    fAllPropIds fileExprs == reverseMapping (fAllPropNames fileExprs)

prop_namesAndIdsRelationsInFileExprs :: FileExprWithMetaDataSpan -> FileExprsWithMetaDataSpan -> Bool
prop_namesAndIdsRelationsInFileExprs fileExpr fileExprs =
    let fileExprs' = addFileExpr fileExpr fileExprs
        fileExpr' = fromJust $ getFileExpr (fPath fileExpr) fileExprs' in
    Map.keysSet (fAllPropIds fileExprs') == Set.union (Map.keysSet $ fAllPropIds fileExprs) (Map.keysSet $ fPropIds fileExpr) &&
    Map.keysSet (fPropIds fileExpr') == Map.keysSet (fPropIds fileExpr) &&
    fAllPropNames fileExprs `Map.isSubmapOf` fAllPropNames fileExprs'

prop_prettyShowReturnsValidLucretiaCode :: FileExprWithMetaDataSpan -> Bool
prop_prettyShowReturnsValidLucretiaCode fileExpr =
    let lucretiaCode = prettyShowFileExpr fileExpr in
    case parseLucretia "test" lucretiaCode of
        Left _ -> False
        Right fileExpr' ->
            haveSameStructure (fExpr fileExpr) (fExpr fileExpr') &&
            Map.keysSet (fPropIds fileExpr) == Map.keysSet (fPropIds fileExpr')

--------- testing functions ---------

parseLucretiaTestCase :: String -> ExprSpan -> String -> Test
parseLucretiaTestCase comment expectedExpression parsedInput =
    TestCase $ case parseLucretia "test" parsedInput of
        Left err -> assertFailure $ comment ++ ": parse error: " ++ err
        Right fileExpr -> assertBool comment $ haveSameStructure expectedExpression $ fExpr fileExpr

evalPositiveAssert importedFiles' comment expectedResult evaluatedExpression =
    case eval importedFiles' evaluatedExpression of
        Left err -> assertFailure $ comment ++ ": runtime error: " ++ prettyShow Map.empty Map.empty err
        Right value -> assertEqual comment expectedResult $ fst value

evalPositiveTestCase :: String -> Value -> ExprSpan -> Test
evalPositiveTestCase comment expectedResult evaluatedExpression =
    TestCase $ evalPositiveAssert emptyFileExprsWithMetaData comment expectedResult evaluatedExpression

evalNegativeAssert comment evaluatedExpression =
    case eval emptyFileExprsWithMetaData evaluatedExpression of
        Left _ -> return ()
        Right _ -> assertFailure $ "Expected failure in test: " ++ comment

evalNegativeTestCase :: String -> ExprSpan -> Test
evalNegativeTestCase comment evaluatedExpression =
    TestCase $ evalNegativeAssert comment evaluatedExpression

resumeEvalPositiveTestCase :: Interpretable e => String -> [Value] -> [e SrcSpan] -> Test
resumeEvalPositiveTestCase comment expectedResults evaluatedExpressions =
    TestCase $ assertEqual comment (map Right expectedResults) (evaluationResults [] evaluatedExpressions NullState) where
        evaluationResults results [] _ = reverse results
        evaluationResults results (expr:exprs) state =
            case resumeEval state emptyFileExprsWithMetaData expr of
                Left err -> Left err : results
                Right (value, state') -> evaluationResults (Right value : results) exprs state'

parseAndEvalLucretiaPositiveTestCase :: String -> Value -> String -> Test
parseAndEvalLucretiaPositiveTestCase comment expectedResult parsedInput =
    TestCase $ case parseLucretia "test" parsedInput of
        Left err -> assertFailure $ comment ++ ": parse error: " ++ err
        Right fileExpr ->
            if Set.null $ importedFilePaths $ fExpr fileExpr then
                evalPositiveAssert emptyFileExprsWithMetaData comment expectedResult $ fExpr fileExpr
            else
                assertFailure $ comment ++ ": unexpected file import"

parseAndEvalLucretiaNegativeTestCase :: String -> String -> Test
parseAndEvalLucretiaNegativeTestCase comment parsedInput =
    TestCase $ case parseLucretia "test" parsedInput of
        Left err -> assertFailure $ comment ++ ": parse error: " ++ err
        Right fileExpr ->
            if Set.null $ importedFilePaths $ fExpr fileExpr then
                evalNegativeAssert comment $ fExpr fileExpr
            else
                assertFailure $ comment ++ ": unexpected file import"

parseAndEvalPython3PositiveTestCase :: FileExprsWithMetaDataSpan -> String -> Value -> String -> Test
parseAndEvalPython3PositiveTestCase builtins comment expectedResult parsedInput =
    TestCase $ case parsePython3 "test" parsedInput of
        Left err -> assertFailure $ comment ++ ": parse error: " ++ err
        Right fileExpr ->
            let testExpr = python3ModuleTestExpr builtins fileExpr in
            case testExpr of
                Left err ->
                    assertFailure $ comment ++ ": " ++ err
                Right (testExpr', _) ->
                    evalPositiveAssert builtins comment expectedResult testExpr'


withPythonBuiltins :: IO t -> (FileExprsWithMetaDataSpan -> IO t) -> IO t
withPythonBuiltins whenErr action = do
    parsedBuiltins <- parsePythonBuiltins
    case parsedBuiltins of
        Left err -> do
            hPutStrLn stderr "I/O error: Failed to load Python builtins."
            whenErr
        Right builtins ->
            action builtins


runUnitTests :: IO Bool
runUnitTests = withPythonBuiltins (return False) $ \builtins -> do
    (counts, _) <- runTestText (putTextToHandle stdout False) (tests builtins)
    return $ errors counts + failures counts == 0



runLucretiaLibTests :: IO Bool
runLucretiaLibTests = do
    libPath <- getLibraryPath
    let testPath = libPath </> "test"
    fileNames <- getDirectoryContents testPath
    let lucFileNames = filter (isSuffixOf "Test.luc") $ map ("%/test/" ++) fileNames
    let testLucretiaFile acc fileName = do
            parseResult <- recursivelyParseLucretiaFiles (Set.singleton fileName) emptyFileExprsWithMetaData
            case parseResult of
                Left err -> do
                    putStrLn $ "Parse error: " ++ err
                    return False
                Right fileExprs -> case eval fileExprs $ fExpr $ fromJust $ getFileExpr fileName fileExprs of
                    Left err -> do
                        putStrLn $ "Runtime error: " ++ prettyShow Map.empty (fAllPropNames fileExprs) err
                        return False
                    Right (value, state) -> case value of
                        StringVal str -> do
                            putStr $ fileName ++ ": " ++ str
                            return $ isPrefixOf "SUCCESS" str && acc
                        _ -> do
                            putStrLn $ "Non-string value returned from test " ++ fileName
                            return False
    foldM testLucretiaFile True lucFileNames


runPythonTests :: IO Bool
runPythonTests = withPythonBuiltins (return False) $ \builtins -> do
    libPath <- getLibraryPath
    let testPath = libPath </> "test"
    fileNames <- getDirectoryContents testPath
    let pyFileNames = filter (isSuffixOf "Test.py") $ map (testPath </>) fileNames
    let testPythonFile acc fileName = do
            testExpr' <- python3FileTestExpr builtins fileName
            case testExpr' of
                Left err -> do
                    putStrLn err
                    return False
                Right (testExpr', _) -> do
                    value <- evalPython3TestExpr builtins testExpr'
                    case value of
                        Left err -> do
                            putStrLn err
                            return False
                        Right (value, _) -> case value of
                            StringVal ones -> do
                                let success = all ('1' ==) ones
                                putStrLn $ fileName ++ ": " ++ (if success then "SUCCESS" else "FAIL") ++ " (" ++ ones ++ ")"
                                return $ success && acc
                            _ -> do
                                putStrLn $ "Non-integer value returned from test " ++ fileName
                                return False
    foldM testPythonFile True pyFileNames

--------- helper functions ---------
similarFloats :: Double -> Double -> Bool
similarFloats lVal rVal = abs (lVal - rVal) < 1e-8 || abs (lVal - rVal) / (abs lVal + abs rVal) < 1e-8

-- returns whether two ASTs have the same structure and literals
-- this check ignores annotations and identifiers
-- for floats approximation is used
haveSameStructure :: Expr annot -> Expr annot -> Bool
haveSameStructure l r = case (l, r) of
    (ConstNone {}, ConstNone {}) -> True
    (ConstBool { boolValue = lVal }, ConstBool { boolValue = rVal }) -> lVal == rVal
    (ConstInt { intValue = lVal }, ConstInt { intValue = rVal }) -> lVal == rVal
    (ConstFloat { floatValue = lVal }, ConstFloat {floatValue = rVal }) -> similarFloats lVal rVal
    (ConstImaginary { imaginaryValue = lVal }, ConstImaginary { imaginaryValue = rVal }) -> similarFloats lVal rVal
    (ConstString { stringValue = lVal }, ConstString { stringValue = rVal }) -> lVal == rVal
    (LetIn { binding = lBinding, body = lBody }, LetIn { binding = rBinding, body = rBody }) ->
        haveSameStructure lBinding rBinding && haveSameStructure lBody rBody
    (Var {}, Var {}) -> True
    (New {}, New {}) -> True
    (Prop {}, Prop {}) -> True
    (AssignProp { value = lValue }, AssignProp { value = rValue }) ->
        haveSameStructure lValue rValue
    (IfElse { cond = lCond, whenTrue = lWhenTrue, whenFalse = lWhenFalse }, IfElse { cond = rCond, whenTrue = rWhenTrue, whenFalse = rWhenFalse }) ->
        haveSameStructure lCond rCond && haveSameStructure lWhenTrue rWhenTrue && haveSameStructure lWhenFalse rWhenFalse
    (IfHasAttr { objExpr = lObjExpr, whenTrue = lWhenTrue, whenFalse = lWhenFalse }, IfHasAttr { objExpr = rObjExpr, whenTrue = rWhenTrue, whenFalse = rWhenFalse }) ->
        haveSameStructure lObjExpr rObjExpr && haveSameStructure lWhenTrue rWhenTrue && haveSameStructure lWhenFalse rWhenFalse
    (Op { args = lArgs }, Op { args = rArgs }) ->
        foldl (\acc (rArg, lArg) -> acc && haveSameStructure rArg lArg) True (zip lArgs rArgs)
    (Fun { body = lBody }, Fun { body = rBody }) ->
        haveSameStructure lBody rBody
    (Appl { fun = lFun, args = lArgs }, Appl { fun = rFun, args = rArgs }) ->
        haveSameStructure lFun rFun && foldl (\acc (rArg, lArg) -> acc && haveSameStructure rArg lArg) True (zip lArgs rArgs)
    (Import { filePath = lFilePath }, Import { filePath = rFilePath }) -> lFilePath == rFilePath
    _ -> False


-- run QuickCheck on all prop_ functions
return [] -- for weird treating of scope in ghc 7.8, have to be after required functions were defined
runInvariantTests :: IO Bool
runInvariantTests =
    $(quickCheckAll' False)
