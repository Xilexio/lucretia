module Test.Tools where

import Test.QuickCheck.All
import Test.QuickCheck.Property as QCP
import Test.QuickCheck.Test as QCT

-- the default $quickCheckAll implementation is not sufficient
quickCheckAll' chatty = [| $(forAllProperties) (quickCheckResult' chatty) |]

quickCheckResult' :: QCP.Testable prop => Bool -> prop -> IO QCT.Result
quickCheckResult' chatty = quickCheckWithResult stdArgs {
        maxSuccess = 200,
        maxSize = 200,
        chatty = chatty
    }
