module Main.Main (
    main
) where

import Control.Monad
import System.Exit (exitFailure, exitSuccess)
import System.IO
import Test.Test


main :: IO ()
main = do
    putStrLn "QuickCheck invariant tests..."
    invariantTestsPassed <- runInvariantTests
    putStrLn $ if invariantTestsPassed then "...PASSED" else "...FAILED"
    putStrLn "HUnit unit tests..."
    unitTestsPassed <- runUnitTests
    putStrLn $ if unitTestsPassed then "...PASSED" else "...FAILED"
    putStrLn "Lucretia library tests..."
    lucLibTestsPassed <- runLucretiaLibTests
    putStrLn $ if lucLibTestsPassed then "...PASSED" else "...FAILED"
    putStrLn "Converted Python tests..."
    pythonTestsPassed <- runPythonTests
    putStrLn $ if pythonTestsPassed then "...PASSED" else "...FAILED"
    if invariantTestsPassed && unitTestsPassed && lucLibTestsPassed && pythonTestsPassed then do
            putStrLn "ALL TESTS PASSED"
            exitSuccess
        else do
            putStrLn "SOME TESTS FAILED"
            exitFailure