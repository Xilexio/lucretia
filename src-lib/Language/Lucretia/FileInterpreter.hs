module Language.Lucretia.FileInterpreter (
    lucretiaFileInterpreter
) where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import Language.Util.Common
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import qualified Data.Set as Set
import Language.Lucretia.Interpreter
import Language.Lucretia.Parser
import Language.Pretty.AST
import Language.Pretty.Common
import Language.Pretty.RuntimeError
import Language.Pretty.Value
import Language.Lucretia.RuntimeError
import System.Exit (exitFailure)
import System.IO

lucretiaFileInterpreter :: String -> IO ()
lucretiaFileInterpreter filePath = do
    parseResult <- recursivelyParseLucretiaFiles (Set.singleton filePath) emptyFileExprsWithMetaData
    case parseResult of
        Left err -> do
            hPutStrLn stderr $ "Parse error: " ++ err
        Right fileExprs -> case eval fileExprs $ fExpr $ fromJust $ getFileExpr filePath fileExprs of
            Left err -> do
                -- TODO get also variable names
                hPutStrLn stderr $ "Runtime error: " ++ prettyShow Map.empty (fAllPropNames fileExprs) err
                exitFailure
            Right (value, state) -> do
                putStrLn $ showValue state 1 (fAllPropNames fileExprs) value ++ " : " ++ showType value
