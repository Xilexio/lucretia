module Language.Lucretia.AST where

import Language.Python.Common.AST (Annotated (..))
import Language.Python.Common.SrcLocation (combineSrcSpans, SrcSpan (..))
import Data.Map (Map)
import qualified Data.Set as Set
import Language.Util.VarMap (VarMap)
import Data.Hashable (Hashable)
import GHC.Generics (Generic)

--------- expressions ---------

-- expressions interpretable line-by-line
-- either expressions or segments (partial let in without body, in order to continue computations later)
data IExpr annot =
    LucretiaExpr { iExpr :: Expr annot } |
    LetSegment { iVarId :: VarIdentAnn annot, iBinding :: Expr annot }
    deriving (Show, Eq, Ord)


-- closed expressions
data Expr annot =
    -- constants
    ConstNone { exprAnnot :: annot } |
    ConstBool { boolValue :: Bool, exprAnnot :: annot } |
    ConstInt { intValue :: Integer, exprAnnot :: annot } |
    ConstFloat { floatValue :: Double, exprAnnot :: annot } |
    ConstImaginary { imaginaryValue :: Double, exprAnnot :: annot } |
    ConstString { stringValue :: String, exprAnnot :: annot } |
    -- let in
    LetIn { varId :: VarIdentAnn annot, binding :: Expr annot, body :: Expr annot, exprAnnot :: annot } |
    Var { varId :: VarIdentAnn annot, exprAnnot :: annot } |
    -- creation of a new empty object
    New { exprAnnot :: annot } |
    -- properties
    Prop { objProp :: ObjPropIdentAnn annot, exprAnnot :: annot } |
    AssignProp { objProp :: ObjPropIdentAnn annot, value :: Expr annot, exprAnnot :: annot } |
    -- if-else
    IfElse { cond :: Expr annot, whenTrue :: Expr annot, whenFalse :: Expr annot, exprAnnot :: annot } |
    -- ifhasattr
    IfHasAttr { objExpr :: Expr annot, prop :: PropIdent, whenTrue :: Expr annot, whenFalse :: Expr annot, exprAnnot :: annot } |
    -- operators
    Op { opType :: OpType, args :: [Expr annot], exprAnnot :: annot } |
    -- functions
    Fun { argVarIds :: [VarIdentAnn annot], body :: Expr annot, exprAnnot :: annot } |
    Appl { fun :: Expr annot, args :: [Expr annot], exprAnnot :: annot } |
    Import { filePath :: String, exprAnnot :: annot }
    deriving (Show, Eq, Ord)

-- identifiers with position annotations
data VarIdentAnn annot = VarIdentAnn { rawVarId :: VarIdent, varAnnot :: annot } deriving (Show, Eq, Ord)
data ObjPropIdentAnn annot = ObjPropIdentAnn { rawObjProp :: ObjPropIdent, objPropAnnot :: annot } deriving (Show, Eq, Ord)


-- types of operators (defined as in Python, where short-circuit logical operators can be instead defined by if-else expression)
data OpType =
    Not |
    Add | Sub | Mul | Div | FloorDiv | Mod | Exp |
    Lesser | LesserOrEq | Greater | GreaterOrEq | Equal | NotEqual | LocEqual | LocNotEqual |
    BitOr | BitAnd | BitXor | BitInv | ShiftLeft | ShiftRight
    deriving (Show, Eq, Ord, Enum, Bounded)


-- ident for variables bound in let in
newtype VarIdent = VarIdent { varIdNum :: Int } deriving (Show, Eq, Ord, Generic)

instance Hashable VarIdent

instance Enum VarIdent where
    toEnum = VarIdent
    fromEnum = varIdNum

-- ident-name of an object property
newtype PropIdent = PropIdent { propIdNum :: Int } deriving (Show, Eq, Ord, Generic)

instance Hashable PropIdent

instance Enum PropIdent where
    toEnum = PropIdent
    fromEnum = propIdNum

-- ident for object and its property
data ObjPropIdent = ObjPropIdent { objVarId :: VarIdent, objPropId :: PropIdent } deriving (Show, Eq, Ord)


instance Annotated IExpr where
    annot LucretiaExpr { iExpr = iExpr } = annot iExpr
    annot LetSegment { iBinding = iBinding } = annot iBinding

instance Annotated Expr where
    annot = exprAnnot

instance Annotated VarIdentAnn where
    annot = varAnnot

instance Annotated ObjPropIdentAnn where
    annot = objPropAnnot

--------- multiple files ---------

-- a file expr - AST of a parsed file along with identifier names
-- there is a redundancy reverseMapping fPropNames == fPropIds
data FileExprWithMetaData annot = FileExprWithMetaData {
        fExpr :: Expr annot,
        fProps :: VarMap String PropIdent,
        fPath :: String
    } deriving (Show, Eq)

-- collection of file exprs
data FileExprsWithMetaData annot = FileExprsWithMetaData {
        fExprs :: Map String (Expr annot),
        fAllProps :: VarMap String PropIdent
    } deriving (Show, Eq)

--------- short type names ---------

type ExprSpan = Expr SrcSpan
type IExprSpan = IExpr SrcSpan
type VarIdentSpan = VarIdentAnn SrcSpan
type ObjPropIdentSpan = ObjPropIdentAnn SrcSpan
type FileExprWithMetaDataSpan = FileExprWithMetaData SrcSpan
type FileExprsWithMetaDataSpan = FileExprsWithMetaData SrcSpan
