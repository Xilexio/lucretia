module Language.Lucretia.ASTFunctions where

import Language.Lucretia.AST
import Language.Util.Common (reverseMapping)
import Data.Maybe (fromJust)
import Data.Tuple (swap)
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Language.Util.VarMap as VarMap

--------- operator functions ---------

arity :: OpType -> [Int]
arity Not = [1]
arity BitInv = [1]
arity Add = [1, 2]
arity Sub = [1, 2]
arity _ = [2]

opSigns = [(Not, "!"),
           (Add, "+"),
           (Sub, "-"),
           (Mul, "*"),
           (Div, "/"),
           (FloorDiv, "//"),
           (Mod, "%"),
           (Exp, "**"),
           (Lesser, "<"),
           (LesserOrEq, "<="),
           (Greater, ">"),
           (GreaterOrEq, ">="),
           (Equal, "=="),
           (NotEqual, "!="),
           (LocEqual, "==="),
           (LocNotEqual, "!=="),
           (BitOr, "|"),
           (BitAnd, "&"),
           (BitXor, "^"),
           (BitInv, "~"),
           (ShiftLeft, "<<"),
           (ShiftRight, ">>")]

opToSign :: OpType -> String
opToSign op = fromJust $ Map.lookup op (Map.fromList opSigns)

signToOp :: String -> Maybe OpType
signToOp sign = Map.lookup sign (Map.fromList $ map swap opSigns)

--------- generic AST functions ---------

-- iterate over all expressions in a tree in pre-order
foldE :: (a -> Expr annot -> a) -> a -> Expr annot -> a
foldE f acc expr = case expr of
    LetIn _ binding body _ -> foldE f (foldE f (f acc expr) binding) body
    AssignProp _ value _ -> foldE f (f acc expr) value
    IfElse cond whenTrue whenFalse _ -> foldE f (foldE f (foldE f (f acc expr) cond) whenTrue) whenFalse
    IfHasAttr objExpr _ whenTrue whenFalse _ -> foldE f (foldE f (foldE f (f acc expr) objExpr) whenTrue) whenFalse
    Op _ args _ -> foldl (foldE f) (f acc expr) args
    Fun _ body _ -> foldE f (f acc expr) body
    Appl fun args _ -> foldl (foldE f) (foldE f (f acc expr) fun) args
    _ -> f acc expr

-- get set of file paths of all imported files in supplied expression
importedFilePaths :: Expr annot -> Set.Set String
importedFilePaths = foldE insertImportedFilePath Set.empty where
    insertImportedFilePath acc Import { filePath = filePath } = Set.insert filePath acc
    insertImportedFilePath acc _ = acc

-- get set of all used PropIdents in supplied expression
usedPropIds :: Expr annot -> Set.Set PropIdent
usedPropIds = foldE insertUsedPropId Set.empty where
    insertUsedPropId acc expr = case expr of
        Prop objPropIdAnn _ -> Set.insert (objPropId $ rawObjProp objPropIdAnn) acc
        AssignProp objPropIdAnn _ _ -> Set.insert (objPropId $ rawObjProp objPropIdAnn) acc
        IfHasAttr _ prop _ _ _ -> Set.insert prop acc
        _ -> acc

-- substitute all PropIdents with different ones in supplied expression
substitutePropIds :: (PropIdent -> PropIdent) -> Expr annot -> Expr annot
substitutePropIds conversion = substitutePropIds' where
    substitutePropIdsInObjProp objPropIdAnn =
        let varId = objVarId $ rawObjProp objPropIdAnn in
        let propId = objPropId $ rawObjProp objPropIdAnn in
        objPropIdAnn {
            rawObjProp = ObjPropIdent {
                    objVarId = varId,
                    objPropId = conversion propId
                }
        }
    substitutePropIds' expr = case expr of
        LetIn varId binding body exprAnnot -> expr {
                binding = substitutePropIds' binding,
                body = substitutePropIds' body
            }
        Prop objPropIdAnn exprAnnot -> expr {
                objProp = substitutePropIdsInObjProp objPropIdAnn
            }
        AssignProp objPropIdAnn value exprAnnot -> expr {
                objProp = substitutePropIdsInObjProp objPropIdAnn,
                value = substitutePropIds' value
            }
        IfElse cond whenTrue whenFalse exprAnnot -> expr {
                cond = substitutePropIds' cond,
                whenTrue = substitutePropIds' whenTrue,
                whenFalse = substitutePropIds' whenFalse
            }
        IfHasAttr objExpr prop whenTrue whenFalse exprAnnot -> expr {
                objExpr = substitutePropIds' objExpr,
                prop = conversion prop,
                whenTrue = substitutePropIds' whenTrue,
                whenFalse = substitutePropIds' whenFalse
            }
        Op opType args exprAnnot -> expr {
                args = map substitutePropIds' args
            }
        Fun argVarIds body exprAnnot -> expr {
                body = substitutePropIds' body
            }
        Appl fun args exprAnnot -> expr {
                fun = substitutePropIds' fun,
                args = map substitutePropIds' args
            }
        _ -> expr

--------- multiple files ---------

emptyFileExprsWithMetaData :: FileExprsWithMetaData annot
emptyFileExprsWithMetaData = FileExprsWithMetaData { fExprs = Map.empty, fAllProps = VarMap.empty }

-- add a file expr to a collection after renaming its PropIdents to ensure no collision
addFileExpr :: FileExprWithMetaData annot -> FileExprsWithMetaData annot -> FileExprsWithMetaData annot
addFileExpr fileExpr fileExprs = FileExprsWithMetaData {
            fExprs = Map.insert (fPath fileExpr) convertedFileExpr $ fExprs fileExprs,
            fAllProps = mergedProps
        } where
    (mergedProps, conversion) = VarMap.merge (fProps fileExpr) (fAllProps fileExprs)
    convertedFileExpr = substitutePropIds conversion $ fExpr fileExpr

-- extract a file expr from a collection of file exprs
getFileExpr :: String -> FileExprsWithMetaData annot -> Maybe (FileExprWithMetaData annot)
getFileExpr filePath fileExprs = case Map.lookup filePath $ fExprs fileExprs of
    Nothing -> Nothing
    Just expr -> let extractedProps = VarMap.filterI (\v -> Set.member v $ usedPropIds expr) (fAllProps fileExprs) in
        Just FileExprWithMetaData {
            fExpr = expr,
            fProps = extractedProps,
            fPath = filePath
        }
