module Language.Lucretia.Interpreter (
    Interpretable (..)
) where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import Language.Util.Common
import Control.Monad (unless)
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Bits (xor, (.&.), (.|.), shiftL, shiftR)
import Data.Complex
import Data.List (splitAt)
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import Data.Tuple (swap)
import Development.Placeholders
import Language.Python.Common.AST (Annotated (..))
import Language.Python.Common.SrcLocation (SrcSpan (..), combineSrcSpans)
import Language.Pretty.AST (prettyShowSrcSpan)
import Language.Lucretia.RunEnvironment
import Language.Lucretia.RuntimeError

type RunEnv = StateT RunEnvState (ExceptT RuntimeErrorWithStackTrace Identity)

----- high-level evaluation functions -----
class Annotated e => Interpretable e where
    evaluator :: e SrcSpan -> RunEnv Value

    eval :: FileExprsWithMetaDataSpan -> e SrcSpan -> Either RuntimeErrorWithStackTrace (Value, RunEnvState)
    eval = resumeEval NullState

    resumeEval :: RunEnvState -> FileExprsWithMetaDataSpan -> e SrcSpan -> Either RuntimeErrorWithStackTrace (Value, RunEnvState)
    resumeEval NullState importedFiles' expr = resumeEval emptyRunEnvState importedFiles' expr
    resumeEval state@(RunEnvState {}) importedFileExprs expr =
        let state' = state { stackTrace = [annot expr] } in
        case runIdentity $ runExceptT $ runStateT (evaluator expr) state' { importedFileExprs = importedFileExprs } of
            Left err -> Left err
            -- after no update was made to the final state
            Right (value, finalState@RunEnvState { finalState = NullState }) -> Right (value, finalState)
            -- after update was made to the final state
            Right (value, RunEnvState { finalState = finalState@(RunEnvState {}) }) -> Right (value, finalState)
            _ -> internalError "unexpected final state"

instance Interpretable Expr where
    evaluator = eval'

instance Interpretable IExpr where
    evaluator = iEval

----- helper functions -----
throwRuntimeError :: RuntimeError -> RunEnv a
throwRuntimeError err = do
    stackTrace' <- gets stackTrace
    throwError $ RuntimeErrorWithStackTrace err stackTrace'

boolVal :: Value -> Bool
boolVal (IntVal val) = val /= 0
boolVal (BoolVal val) = val
boolVal (FloatVal val) = val /= 0
boolVal (ComplexVal val) = val /= 0
boolVal (StringVal val) = val /= ""
boolVal (ObjectVal _) = True
boolVal (FunVal _ _) = True
boolVal NoneVal = False

numVal True = 1
numVal False = 0

getVar :: VarIdent -> SrcSpan -> RunEnv Value
getVar varId srcSpan = do
    vars' <- gets vars
    case Map.lookup varId vars' of
        Nothing -> throwRuntimeError $ VarNotInScope varId srcSpan
        Just val -> return val

toCommon :: (Value, Value) -> (Value, Value)
toCommon (l, r) = case (l, r) of
    (BoolVal _, BoolVal _) -> (l, r)
    _ -> toCommonNum (l, r)

-- this converts both argument types to common type if they are numeric and converts Bool to Int
toCommonNum :: (Value, Value) -> (Value, Value)
toCommonNum (l, r) = case (l, r) of
    (ComplexVal _, ComplexVal _) -> (l, r)
    (ComplexVal lVal, IntVal rVal) -> (l, ComplexVal $ fromInteger rVal)
    (ComplexVal lVal, FloatVal rVal) -> (l, ComplexVal $ rVal :+ 0)
    (_, ComplexVal _) -> swap $ toCommonNum (r, l)
    (FloatVal _, FloatVal _) -> (l, r)
    (FloatVal lVal, IntVal rVal) -> (l, FloatVal $ fromIntegral rVal)
    (_, FloatVal _) -> swap $ toCommonNum (r, l)
    (BoolVal lVal, _) -> toCommonNum (IntVal $ if lVal then 1 else 0, r)
    (_, BoolVal _) -> swap $ toCommonNum (r, l)
    _ -> (l, r)

getObjectLoc :: VarIdent -> SrcSpan -> RunEnv Location
getObjectLoc varId srcSpan = do
    object <- getVar varId srcSpan
    case object of
        ObjectVal loc -> return loc
        _ -> throwRuntimeError $ DotOnNonObject varId srcSpan

----- evaluation of closed expressions -----

eval' :: ExprSpan -> RunEnv Value
eval' ConstInt { intValue = intValue } = return $ IntVal intValue
eval' ConstBool { boolValue = boolValue } = return $ BoolVal boolValue
eval' ConstFloat { floatValue = floatValue } = return $ FloatVal floatValue
eval' ConstImaginary { imaginaryValue = imaginaryValue } = return $ ComplexVal $ 0 :+ imaginaryValue
eval' ConstString { stringValue = stringValue } = return $ StringVal stringValue
eval' ConstNone {} = return NoneVal

eval' Var { varId = varId, exprAnnot = srcSpan } = getVar (rawVarId varId) srcSpan

eval' Op { opType = opType, args = args, exprAnnot = srcSpan } = do
    unless (elem (length args) $ arity opType) $ throwRuntimeError $ InvalidOpArgCount opType srcSpan
    argVals <- mapM eval' args
    -- preprocessing and changing to common numerical type
    -- some operations are changed to 2-arg ones
    let (origL, origR) = case argVals of
            [a] -> case opType of
                Not -> (a, internalError "used dummy argument")
                Add -> (IntVal 0, a)
                Sub -> (IntVal 0, a)
                BitInv -> (a, internalError "used dummy argument")
            [l, r] -> (l, r)
    let (l', r') = toCommon (origL, origR)
    let (l, r) = toCommonNum (origL, origR)
    let neg x = case x of
            IntVal val -> return $ IntVal (-val)
            FloatVal val -> return $ FloatVal (-val)
            ComplexVal val -> return $ ComplexVal (-val)
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let add (l, r) = case (l, r) of
            (StringVal lVal, StringVal rVal) -> return $ StringVal $ lVal ++ rVal
            (ComplexVal lVal, ComplexVal rVal) -> return $ ComplexVal $ lVal + rVal
            (FloatVal lVal, FloatVal rVal) -> return $ FloatVal $ lVal + rVal
            (IntVal lVal, IntVal rVal) -> return $ IntVal $ lVal + rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let mul (l, r) = case (l, r) of
            (StringVal lVal, IntVal rVal) -> return $ StringVal $ concat $ replicate (fromIntegral rVal) lVal
            (ComplexVal lVal, ComplexVal rVal) -> return $ ComplexVal $ lVal * rVal
            (FloatVal lVal, FloatVal rVal) -> return $ FloatVal $ lVal * rVal
            (IntVal lVal, IntVal rVal) -> return $ IntVal $ lVal * rVal
            (IntVal _, StringVal _) -> mul (r, l)
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let division (l, r) = case (l, r) of
            (ComplexVal lVal, ComplexVal rVal) -> do
                unless (rVal /= 0) $ throwRuntimeError $ DivisionByZero opType srcSpan
                return $ ComplexVal $ lVal / rVal
            (FloatVal lVal, FloatVal rVal) -> do
                unless (rVal /= 0) $ throwRuntimeError $ DivisionByZero opType srcSpan
                return $ FloatVal $ lVal / rVal
            (IntVal lVal, IntVal rVal) -> division (FloatVal $ fromInteger lVal, FloatVal $ fromInteger rVal)
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    -- division with floor, defined in http://docs.python.org/2/reference/expressions.html#binary-arithmetic-operations
    let floorDiv (l, r) = case (l, r) of
            (FloatVal lVal, FloatVal rVal) -> return $ FloatVal $ fromInteger $ floor $ lVal / rVal
            (IntVal lVal, IntVal rVal) -> return $ IntVal $ lVal `div` rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    -- modulo defined in http://docs.python.org/2/reference/expressions.html#binary-arithmetic-operations
    -- description yields that modulo for floats and integers is: a % b = a - floor(a / b)
    -- in Haskell mod is the same as in Python for integers (though it's not defined for floats)
    let modulo (l, r) = case (l, r) of
            (FloatVal lVal, FloatVal rVal) -> do
                unless (rVal /= 0) $ throwRuntimeError $ DivisionByZero opType srcSpan
                return $ FloatVal $ lVal - fromInteger (floor (lVal / rVal)) * rVal
            (IntVal lVal, IntVal rVal) -> do
                unless (rVal /= 0) $ throwRuntimeError $ DivisionByZero opType srcSpan
                return $ IntVal $ lVal `mod` rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    -- TODO division by zero and imaginary numbers in exp
    let exp (l, r) = case (l, r) of
            (ComplexVal lVal, ComplexVal rVal) -> return $ ComplexVal $ lVal ** rVal
            (FloatVal lVal, FloatVal rVal) -> return $ FloatVal $ lVal ** rVal
            (IntVal lVal, IntVal rVal) -> return $ IntVal $ lVal ^ rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let bitor (l, r) = case (l, r) of
            (BoolVal lVal, BoolVal rVal) -> return $ BoolVal $ lVal || rVal
            (IntVal lVal, IntVal rVal) -> return $ IntVal $ lVal .|. rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let bitand (l, r) = case (l, r) of
            (BoolVal lVal, BoolVal rVal) -> return $ BoolVal $ lVal && rVal
            (IntVal lVal, IntVal rVal) -> return $ IntVal $ lVal .&. rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let bitxor (l, r) = case (l, r) of
            (BoolVal lVal, BoolVal rVal) -> return $ BoolVal $ if lVal then not rVal else rVal
            (IntVal lVal, IntVal rVal) -> return $ IntVal $ lVal `xor` rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    -- bitwise inversion defined in http://docs.python.org/2/reference/expressions.html#unary-arithmetic-and-bitwise-operations
    let bitinv x = case x of
            IntVal val -> return $ IntVal $ -(val+1)
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let shiftLeft (l, r) = case (l, r) of
            (IntVal lVal, IntVal rVal) -> do
                unless (rVal >= 0) $ throwRuntimeError $ NegativeShiftOffset opType srcSpan
                return $ IntVal $ shiftL lVal $ fromInteger rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    let shiftRight (l, r) = case (l, r) of
            (IntVal lVal, IntVal rVal) -> do
                unless (rVal >= 0) $ throwRuntimeError $ NegativeShiftOffset opType srcSpan
                return $ IntVal $ shiftR lVal $ fromInteger rVal
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    -- comparisons are defined in http://docs.python.org/2/reference/expressions.html#not-in
    let cmpOp op (l, r) = case (l, r) of
            (StringVal _, StringVal _) -> return $ BoolVal $ op l r
            (FloatVal _, FloatVal _) -> return $ BoolVal $ op l r
            (IntVal _, IntVal _) -> return $ BoolVal $ op l r
            _ -> throwRuntimeError $ InvalidOpArg opType srcSpan
    -- by default objects have location equality
    -- functions are incomparable in Lucretia, though one may create function objects for them to behave like in Python
    let eqOp op (l, r) = case (l, r) of
            (FunVal _ _, FunVal _ _) -> return $ BoolVal False
            _ -> return $ BoolVal $ op l r
    case opType of
        Not -> return $ BoolVal $ not $ boolVal l'
        Add -> add (l, r)
        Sub -> do
            negR <- neg r
            add (l, negR)
        Mul -> mul (l, r)
        Div -> division (l, r)
        FloorDiv -> floorDiv (l, r)
        Mod -> modulo (l, r)
        Exp -> exp (l, r)
        Lesser -> cmpOp (<) (l, r)
        LesserOrEq -> cmpOp (<=) (l, r)
        Greater -> cmpOp (>) (l, r)
        GreaterOrEq -> cmpOp (>=) (l, r)
        Equal -> eqOp (==) (l, r)
        NotEqual -> eqOp (/=) (l, r)
        -- in "is" operator in Python, literals can behave differently after operating on them
        -- we assume normal equality for this operator (that is same literals have same objects)
        -- in practice in Python small non-complex numbers and strings have same locations,
        -- big ones have different locations and imaginary numbers have different location after any operation
        -- documented in http://docs.python.org/2/reference/expressions.html#literals
        LocEqual -> eqOp (==) (origL, origR)
        LocNotEqual -> eqOp (/=) (origL, origR)
        BitOr -> bitor (l', r')
        BitAnd -> bitand (l', r')
        BitXor -> bitxor (l', r')
        BitInv -> bitinv l
        ShiftLeft -> shiftLeft (l, r)
        ShiftRight -> shiftRight (l, r)

eval' LetIn { varId = varId, binding = binding, body = body } = do
    bindingVal <- eval' binding
    vars' <- gets vars
    modify $ \s -> s { vars = Map.insert (rawVarId varId) bindingVal vars' }
    result <- eval' body
    modify $ \s -> s { vars = vars' }
    return result

eval' New {} = do
    objects' <- gets objects
    let loc = Map.size objects'
    modify $ \s -> s { objects = Map.insert loc Map.empty objects' }
    return $ ObjectVal loc

eval' Prop { objProp = objProp, exprAnnot = srcSpan} = do
    loc <- getObjectLoc (objVarId $ rawObjProp objProp) srcSpan
    objects' <- gets objects
    let objectMap = fromJust $ Map.lookup loc objects'
    case Map.lookup (objPropId $ rawObjProp objProp) objectMap of
        Nothing -> throwRuntimeError $ NonexistentPropReferenced (rawObjProp objProp) srcSpan
        Just val -> return val

eval' AssignProp { objProp = objProp, value = value, exprAnnot = srcSpan } = do
    val <- eval' value
    loc <- getObjectLoc (objVarId $ rawObjProp objProp) srcSpan
    objects' <- gets objects
    let objectMap = fromJust $ Map.lookup loc objects'
    let updatedObjects = Map.insert loc (Map.insert (objPropId $ rawObjProp objProp) val objectMap) objects'
    modify $ \s -> s { objects = updatedObjects }
    return val

eval' IfElse { cond = cond, whenTrue = whenTrue, whenFalse = whenFalse } = do
    condVal <- eval' cond
    if boolVal condVal then eval' whenTrue else eval' whenFalse

eval' IfHasAttr { objExpr = objExpr, prop = propId, whenTrue = whenTrue, whenFalse = whenFalse } = do
    objVal <- eval' objExpr
    hasProp <- case objVal of
        ObjectVal loc -> do
            objects' <- gets objects
            let objectMap = fromJust $ Map.lookup loc objects'
            return $ Map.member propId objectMap
        _ -> return False
    if hasProp then eval' whenTrue else eval' whenFalse

eval' fun@(Fun {}) = do
    vars' <- gets vars
    return FunVal { funExpr = fun, runVars = vars' }

eval' Appl { fun = fun, args = args, exprAnnot = srcSpan } = do
    funVal <- eval' fun
    case funVal of
        FunVal { funExpr = Fun { argVarIds = argVarIds, body = body }, runVars = runVars } -> do
            unless (length argVarIds == length args) $ throwRuntimeError $ InvalidFunArgCount srcSpan
            argVals <- mapM eval' args
            let varUpdate = zip (map rawVarId argVarIds) argVals
            vars' <- gets vars
            modify $ \s -> s { vars = Map.union (Map.fromList varUpdate) runVars, stackTrace = srcSpan : stackTrace s }
            result <- eval' body
            modify $ \s -> s { vars = vars', stackTrace = tail $ stackTrace s }
            return result
        _ -> throwRuntimeError $ ApplOnNonFun srcSpan

eval' (Import { filePath = filePath, exprAnnot = srcSpan }) = do
    importedFileExprs' <- gets importedFileExprs
    case Map.lookup filePath $ fExprs importedFileExprs' of
        Just expr -> do
            vars' <- gets vars
            modify $ \s -> s { vars = Map.empty, stackTrace = exprAnnot expr : srcSpan : stackTrace s }
            result <- eval' expr
            modify $ \s -> s { vars = vars', stackTrace = drop 2 $ stackTrace s }
            return result
        Nothing -> throwRuntimeError $ ImportNotParsed filePath srcSpan

----- evaluation of interpreted expressions -----

iEval :: IExprSpan -> RunEnv Value
iEval (LucretiaExpr expr) = eval' expr

-- evaluating closed expression equivalent in terms of final state and result
iEval (LetSegment iVarId iBinding) = do
    bindingVal <- eval' iBinding
    vars' <- gets vars
    modify $ \s -> s { vars = Map.insert (rawVarId iVarId) bindingVal vars' }
    result <- eval' $ Var iVarId SpanEmpty
    modify $ \s -> s { finalState = NullState }
    finalState <- get
    modify $ \s -> s { vars = vars', finalState = finalState }
    return result
