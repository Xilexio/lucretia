module Language.Lucretia.InteractiveInterpreter (
    lucretiaInteractiveInterpreter
) where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import Language.Util.Common
import Control.Monad.State
import qualified Data.Map as Map
import qualified Data.Set as Set
import Language.Lucretia.Interpreter
import Language.Lucretia.Parser
import Language.Pretty.AST
import Language.Pretty.Common
import Language.Pretty.RuntimeError
import Language.Pretty.Value
import Language.Lucretia.RunEnvironment (emptyRunEnvState, Location, locationEmpty, RunEnvState (..), Value (..))
import Language.Lucretia.RuntimeError
import System.Exit (exitFailure, exitSuccess)
import System.IO

lucretiaInteractiveInterpreter :: IO ()
lucretiaInteractiveInterpreter = do
    hSetBuffering stdout NoBuffering
    putStrLn $ "Lucretia interpreter v" ++ programVer
    putStrLn $ ":? for help, :q to quit"
    evalStateT interpreterLoop (emptyRunEnvState, emptyLucEnvState)

showValueWithoutType :: Int -> RunEnvState -> LucEnvState -> Value -> String
showValueWithoutType depth runEnvState lucEnvState value =
    showValue runEnvState depth (reverseMapping $ propIds lucEnvState) value

showValueWithType :: RunEnvState -> LucEnvState -> Value -> String
showValueWithType runEnvState lucEnvState value =
    showValueWithoutType 1 runEnvState lucEnvState value ++ " : " ++ showType value

prettyShow' :: PrettyShow t => LucEnvState -> t -> String
prettyShow' lucEnvState = prettyShow (reverseMapping $ varIds lucEnvState) (reverseMapping $ propIds lucEnvState)

printHeapDump :: StateT (RunEnvState, LucEnvState) IO ()
printHeapDump = do
    liftIO $ putStrLn "Heap dump:"
    runEnvState <- gets fst
    case Map.null $ objects runEnvState of
        True -> liftIO $ putStrLn "empty"
        False -> do
            lucEnvState <- gets snd
            forM_ (Map.keys $ objects runEnvState) $ liftIO . putStrLn . (showValueWithType runEnvState lucEnvState) . ObjectVal

printVariables :: StateT (RunEnvState, LucEnvState) IO ()
printVariables = do
    liftIO $ putStrLn "Variables:"
    runEnvState <- gets fst
    let varIdsInScope = Set.fromList $ Map.keys $ vars runEnvState
    lucEnvState <- gets snd
    let varNamesInScope = Map.keys $ Map.filter (\varId -> Set.member varId varIdsInScope) $ varIds lucEnvState
    case varNamesInScope of
        [] -> liftIO $ putStrLn "empty"
        _ -> forM_ varNamesInScope $ liftIO . putStrLn

printObjDump :: Location -> Int -> StateT (RunEnvState, LucEnvState) IO ()
printObjDump loc depth = do
    runEnvState <- gets fst
    lucEnvState <- gets snd
    liftIO $ putStrLn $ case locationEmpty loc runEnvState of
        True -> "Error: no object exists at location " ++ show loc
        False -> showValueWithoutType depth runEnvState lucEnvState $ ObjectVal loc

evalInput :: String -> StateT (RunEnvState, LucEnvState) IO ()
evalInput input = do
    lucEnvState <- gets snd
    case parseInterpretedLucretia "stdin" input lucEnvState of
        Left err -> do
            liftIO $ putStrLn $ "Parse error: " ++ err
        Right (iExpr, lucEnvState') -> do
            runEnvState <- gets fst
            let newExpr = case iExpr of
                    LucretiaExpr { iExpr = expr } -> expr
                    LetSegment { iBinding = expr } -> expr
            let newFilePaths = Set.difference (importedFilePaths newExpr) (Map.keysSet $ fExprs $ importedFileExprs runEnvState)
            let fileExprs' = importedFileExprs runEnvState
            importResult <- case Set.null newFilePaths of
                True -> return $ Right (fileExprs', lucEnvState')
                False -> do
                    -- updating fAllPropIds so no propId collision in new files will occur
                    let propIds' = Map.union (fAllPropIds fileExprs') (propIds lucEnvState')
                    let fileExprs'' = fileExprs' {
                            fAllPropNames = reverseMapping propIds',
                            fAllPropIds = propIds'
                        }
                    -- recursively parsing newly imported files
                    parseResult <- liftIO $ recursivelyParseLucretiaFiles newFilePaths fileExprs''
                    case parseResult of
                        Left err -> do
                            liftIO $ putStrLn $ "Parse error: " ++ err
                            return $ Left ()
                        Right fileExprs''' -> do
                            -- copying fAllPropIds into lucEnvState, so no propId collisions in subsequently interpreted lines will happen
                            let lucEnvState'' = lucEnvState' { propIds = Map.union (fAllPropIds fileExprs''') (propIds lucEnvState') }
                            return $ Right (fileExprs''', lucEnvState'')
            case importResult of
                Left () -> return ()
                Right (fileExprs, lucEnvState'') -> case resumeEval runEnvState fileExprs iExpr of
                    Left err -> do
                        -- TODO get also variable names from imported files
                        liftIO $ putStrLn $ "Runtime error: " ++ prettyShow' lucEnvState'' err
                    Right (value, runEnvState') -> do
                        liftIO $ putStrLn $ showValueWithType runEnvState' lucEnvState'' value
                        put (runEnvState', lucEnvState'')

interpreterLoop :: StateT (RunEnvState, LucEnvState) IO ()
interpreterLoop = do
    liftIO $ putStr "> "
    input <- liftIO getLine
    case words input of
        [":q"] -> liftIO exitSuccess
        [":?"] -> liftIO $ putStrLn $ unlines [
                ":?                        Show this help",
                ":heap                     Dumps heap",
                ":obj <loc> <depth>        Dumps object properties recursively with given depth",
                ":q                        Quits interpreter",
                ":vars                     Prints names of variables in scope"
            ]
        [":heap"] -> printHeapDump
        [":obj", locStr, depthStr] -> case ((reads locStr)::[(Int, String)], (reads depthStr)::[(Int, String)]) of
            ([(loc, "")], [(depth, "")]) | loc >= 0 && depth >= 0 -> printObjDump loc depth
            _ -> liftIO $ putStrLn "Location and depth arguments must be non-negative numbers"
        [":vars"] -> printVariables
        (':':cmd):_ -> do
            liftIO $ putStrLn $ "Unknown command \"" ++ cmd ++ "\" or wrong number of parameters"
        _ -> evalInput input
    interpreterLoop
