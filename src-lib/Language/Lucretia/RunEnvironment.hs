{-# LANGUAGE FlexibleInstances #-}

module Language.Lucretia.RunEnvironment where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import Language.Util.Common
import Control.Monad.State
import Data.Complex
import qualified Data.Map as Map
import Language.Python.Common.SrcLocation (SrcSpan (..))

data Value =
    IntVal Integer |
    BoolVal Bool |
    FloatVal Double |
    ComplexVal (Complex Double) |
    StringVal String |
    NoneVal |
    ObjectVal { location :: Location } |
    FunVal { funExpr :: ExprSpan, runVars :: Map.Map VarIdent Value }
    deriving (Show, Eq)

type Location = Int

-- vars - map of variables (created in let in) from their identificators into their values
-- objects - heap containing objects in form of map from locations into map of object properties and their values
-- finalState - last state after evaluating body of let in - can be used to efficiently resume computations
-- NullState is used to not remember more than one last state after let in evaluation. In high-level functions it'll be treated as
-- empty state, otherwise will result in errors. When using interpreter, it's safer to use emptyRunEnvState instead of NullState.
-- Resuming computations (extending with expression Z) can be done by changing let V1 = X in Y into let V1 = X in let V2 = Y in Z.
-- If base computation was not a let in expression, it will be independent of new expression anyway
-- (besides changing inaccessible part of heap in case of operator new). So we can ignore remembering state in non-let in expressions.
data RunEnvState = NullState | RunEnvState {
        vars :: Map.Map VarIdent Value,
        objects :: Map.Map Location (Map.Map PropIdent Value),
        finalState :: RunEnvState,
        importedFileExprs :: FileExprsWithMetaDataSpan,
        stackTrace :: [SrcSpan]
    }

createRunEnvState :: Map.Map String ExprSpan -> RunEnvState
createRunEnvState importedFiles' = RunEnvState {
        vars = Map.empty,
        objects = Map.empty,
        finalState = NullState,
        importedFileExprs = emptyFileExprsWithMetaData,
        stackTrace = []
    }

emptyRunEnvState :: RunEnvState
emptyRunEnvState = createRunEnvState Map.empty

locationEmpty :: Location -> RunEnvState -> Bool
locationEmpty loc runEnvState = not $ Map.member loc $ objects runEnvState

instance Ord Value where
    compare (IntVal lVal) (IntVal rVal) = compare lVal rVal
    compare (BoolVal lVal) (BoolVal rVal) = compare lVal rVal
    compare (FloatVal lVal) (FloatVal rVal) = compare lVal rVal
    compare (StringVal lVal) (StringVal rVal) = compare lVal rVal
    compare _ _ = internalError "incomparable Value types"
