module Language.Lucretia.Parser (
    emptyLucEnvState,
    LucEnvState (..),
    parseLucretiaFile,
    parseInterpretedLucretia,
    parseLucretia,
    recursivelyParseLucretiaFiles
) where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import Language.Util.Common
import Control.Monad.Identity
import Control.Monad.State.Strict (StateT, evalStateT, get)
import Control.Lens
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.Set as Set
import Development.Placeholders
import Language.Util.ImportFileIO
import Language.Python.Common.AST (Annotated (..))
import Language.Python.Common.SrcLocation
import Language.Util.ParserCommon
import qualified Text.Parsec as Parsec
import Text.Parsec (ParsecT, SourcePos, (<|>), runParserT, try)
import qualified Text.Parsec.Token as PT
import qualified Text.Parsec.Char as PC
import Text.Parsec.Language (haskellStyle)
import Language.Lucretia.Types
import Language.Util.VarMap (VarMap, (<@+=))
import qualified Language.Util.VarMap as VarMap


data LucEnvState = LucEnvState {
        _vars :: VarMap String VarIdent,
        _props :: VarMap String PropIdent
    }
makeLenses ''LucEnvState

emptyLucEnvState :: LucEnvState
emptyLucEnvState = LucEnvState { _vars = VarMap.empty, _props = VarMap.empty }

type LucretiaLanguageDef = PT.GenLanguageDef String () (StateT LucEnvState Identity)
type LucretiaTokenParser = PT.GenTokenParser String () (StateT LucEnvState Identity)
type LucretiaParser t = ParsecT String () (StateT LucEnvState Identity) t

----- high-level functions -----
-- TODO proper behavior for cycles when using import - in Python import and import from are different cases
recursivelyParseLucretiaFiles :: Set.Set String -> FileExprsWithMetaDataSpan -> IO (Either String FileExprsWithMetaDataSpan)
recursivelyParseLucretiaFiles addedFilePaths fileExprs = do
    let importedFilesToParse = Map.foldr (Set.union . importedFilePaths) Set.empty $ fExprs fileExprs
    let allFilesToParse = Set.union importedFilesToParse addedFilePaths
    let unparsedFiles = Set.difference allFilesToParse $ Map.keysSet $ fExprs fileExprs
    recursivelyParseLucretiaFiles' fileExprs unparsedFiles where
        recursivelyParseLucretiaFiles' fileExprs unparsedFiles =
            if Set.null unparsedFiles then
                return $ Right fileExprs
            else do
                let filePath = Set.findMin unparsedFiles
                out <- parseLucretiaFile filePath
                case out of
                    Left err -> return $ Left err
                    Right fileExpr -> do
                        let fileExprs' = addFileExpr fileExpr fileExprs
                        let importedFilePaths' = importedFilePaths $ fExpr fileExpr
                        let unparsedFiles' = Set.difference (Set.union unparsedFiles importedFilePaths') $ Map.keysSet $ fExprs fileExprs'
                        recursivelyParseLucretiaFiles' fileExprs' unparsedFiles'

parseLucretiaFile :: String -> IO (Either String FileExprWithMetaDataSpan)
parseLucretiaFile filePath = do
    importedContents <- readImportedFile filePath
    case importedContents of
        Left err -> return $ Left $ "I/O error: " ++ show err
        Right contents -> return $ parseLucretia filePath contents

parseInterpretedLucretia :: String -> String -> LucEnvState -> Either String (IExprSpan, LucEnvState)
parseInterpretedLucretia = parseAndShowError pInterpreted

parseLucretia :: String -> String -> Either String FileExprWithMetaDataSpan
parseLucretia sourceName input = case parseAndShowError pProg sourceName input emptyLucEnvState of
    Left err -> Left err
    Right (expr, lucEnvState) ->
        Right FileExprWithMetaData {
            fExpr = expr,
            fProps = view props lucEnvState,
            fPath = sourceName
        }

parseAndShowError :: LucretiaParser a -> String -> String -> LucEnvState -> Either String a
parseAndShowError parser sourceName input lucEnvState =
    case runIdentity $ flip evalStateT lucEnvState $ runParserT parser () sourceName input of
        Left err -> Left $ show err
        Right output -> Right output

----- lexer -----
langDef :: LucretiaLanguageDef
langDef = PT.LanguageDef {
        PT.commentStart = "{-",
        PT.commentEnd = "-}",
        PT.commentLine = "--",
        PT.nestedComments = True,
        PT.identStart = PC.letter <|> PC.char '_',
        PT.identLetter = Parsec.alphaNum <|> Parsec.oneOf "_'",
        PT.opStart = PT.opLetter langDef,
        PT.opLetter = Parsec.oneOf ":!#$%&*+./<=>?@\\^|-~",
        PT.reservedOpNames = [],
        PT.reservedNames = ["let", "in", "if", "then", "else", "ifhasattr", "new", "None", "True", "False", "func", "import"],
        PT.caseSensitive = True
    }

lexer :: LucretiaTokenParser
lexer = PT.makeTokenParser langDef

----- parser -----
posToSrcLoc :: SourcePos -> SrcLocation
posToSrcLoc pos = Sloc {
        sloc_filename = Parsec.sourceName pos,
        sloc_row = Parsec.sourceLine pos,
        sloc_column = Parsec.sourceColumn pos
    }

getSrcLoc :: LucretiaParser SrcLocation
getSrcLoc = do
    pos <- Parsec.getPosition
    return $ posToSrcLoc pos

addSrcSpan :: LucretiaParser (SrcSpan -> t) -> LucretiaParser t
addSrcSpan exprWithoutSrcSpan = do
    loc1 <- getSrcLoc
    e <- exprWithoutSrcSpan
    loc2 <- getSrcLoc
    return $ e $ mkSrcSpan loc1 $ decColumn 1 loc2

pVarIdent :: LucretiaParser VarIdent
pVarIdent = do
    name <- PT.identifier lexer
    vars <@+= name

pPropIdent :: LucretiaParser PropIdent
pPropIdent = do
    name <- PT.identifier lexer
    props <@+= name

pVarIdentAnn :: LucretiaParser VarIdentSpan
pVarIdentAnn = addSrcSpan $ do
    varId <- pVarIdent
    return $ VarIdentAnn varId

pInterpreted :: LucretiaParser (IExprSpan, LucEnvState)
pInterpreted = try pLetSegment <|> pLucretiaExpr where
    pLetSegment = do
        PT.whiteSpace lexer
        PT.reserved lexer "let"
        varId <- pVarIdentAnn
        PT.reservedOp lexer "="
        binding <- pExpr'
        Parsec.eof
        state <- get
        return (LetSegment { iVarId = varId, iBinding = binding }, state)
    pLucretiaExpr = do
        (expr, state') <- pProg
        return (LucretiaExpr { iExpr = expr }, state')

pProg :: LucretiaParser (ExprSpan, LucEnvState)
pProg = do
    PT.whiteSpace lexer
    expr <- pExpr
    Parsec.eof
    state <- get
    return (expr, state)

pExpr :: LucretiaParser ExprSpan
pExpr = try pSemi <|>
        try pLetInSemi <|>
        pExpr'

pExpr' :: LucretiaParser ExprSpan
pExpr' = try pAppl <|>
        pNone <|>
        pBool <|>
        try pNum <|>
        pString <|>
        pImport <|>
        pLetIn <|>
        try pAssignProp <|>
        try pProp <|>
        pVar <|>
        pNew <|>
        pIfElse <|>
        pIfHasAttr <|>
        pOp <|>
        pFun <|>
        PT.parens lexer pExpr

pLetInSemi :: LucretiaParser ExprSpan
pLetInSemi = addSrcSpan $ do
    PT.reserved lexer "let"
    varId <- pVarIdentAnn
    PT.reservedOp lexer "="
    binding <- pExpr'
    PT.reserved lexer ";"
    body <- pExpr
    return $ \srcSpan -> LetIn {
            varId = varId,
            binding = binding,
            body = body,
            exprAnnot = srcSpan
        }

pSemi :: LucretiaParser ExprSpan
pSemi = addSrcSpan $ do
    varId <- VarMap.freshVar vars
    binding <- pExpr'
    PT.reserved lexer ";"
    body <- pExpr
    return $ \srcSpan -> LetIn {
            varId = buildVarId varId,
            binding = binding,
            body = body,
            exprAnnot = srcSpan
        }

pLetIn :: LucretiaParser ExprSpan
pLetIn = addSrcSpan $ do
    PT.reserved lexer "let"
    varId <- pVarIdentAnn
    PT.reservedOp lexer "="
    binding <- pExpr'
    PT.reserved lexer "in"
    body <- pExpr'
    return $ \srcSpan -> LetIn {
            varId = varId,
            binding = binding,
            body = body,
            exprAnnot = srcSpan
        }

pVar :: LucretiaParser ExprSpan
pVar = addSrcSpan $ do
    varId <- pVarIdentAnn
    return $ Var varId

pNew :: LucretiaParser ExprSpan
pNew = addSrcSpan $ do
    PT.reserved lexer "new"
    return New

pObjPropIdChain :: LucretiaParser (ExprSpan -> ExprSpan, ObjPropIdentSpan)
pObjPropIdChain = do
    objVarId <- pVarIdent
    PT.dot lexer
    let pPropIdentWithSrcSpan = addSrcSpan $ do
            propId <- pPropIdent
            return $ \srcSpan -> (propId, srcSpan)
    propChain <- Parsec.sepBy1 pPropIdentWithSrcSpan (PT.dot lexer)
    let propExpr exprSegment varId propIds =
            case propIds of
                [propId] -> return (exprSegment, ObjPropIdentAnn (ObjPropIdent varId $ fst propId) $ snd propId)
                propId:propIds' -> do
                    tempVar <- VarMap.freshVar vars
                    let exprSegment' = exprSegment . \expr -> LetIn {
                            varId = buildVarId tempVar,
                            binding = Prop {
                                    objProp = ObjPropIdentAnn (ObjPropIdent varId $ fst propId) $ snd propId,
                                    exprAnnot = snd propId
                                },
                            body = expr,
                            exprAnnot = combineSrcSpans (exprAnnot expr) (snd propId)
                        }
                    propExpr exprSegment' tempVar propIds'
    propExpr id objVarId propChain

pProp :: LucretiaParser ExprSpan
pProp = addSrcSpan $ do
    objProp <- pObjPropIdChain
    tempVar <- VarMap.freshVar vars
    return $ \srcSpan -> fst objProp Prop {
            objProp = snd objProp,
            exprAnnot = srcSpan
        }

pAssignProp :: LucretiaParser ExprSpan
pAssignProp = addSrcSpan $ do
    objProp <- pObjPropIdChain
    PT.reservedOp lexer "="
    value <- pExpr'
    tempVar <- VarMap.freshVar vars
    return $ \srcSpan -> fst objProp AssignProp {
            objProp = snd objProp,
            value = value,
            exprAnnot = srcSpan
        }

pIfElse :: LucretiaParser ExprSpan
pIfElse = addSrcSpan $ do
    PT.reserved lexer "if"
    cond <- pExpr'
    PT.reserved lexer "then"
    whenTrue <- pExpr'
    PT.reserved lexer "else"
    whenFalse <- pExpr'
    return $ \srcSpan -> IfElse {
            cond = cond,
            whenTrue = whenTrue,
            whenFalse = whenFalse,
            exprAnnot = srcSpan
        }

pIfHasAttr :: LucretiaParser ExprSpan
pIfHasAttr = addSrcSpan $ do
    PT.reserved lexer "ifhasattr"
    (objExpr, propId) <- PT.parens lexer $ do
        objExpr <- pExpr'
        PT.comma lexer
        propId <- pPropIdent
        return (objExpr, propId)
    PT.reserved lexer "then"
    whenTrue <- pExpr'
    PT.reserved lexer "else"
    whenFalse <- pExpr'
    return $ \srcSpan -> IfHasAttr {
            objExpr = objExpr,
            prop = propId,
            whenTrue = whenTrue,
            whenFalse = whenFalse,
            exprAnnot = srcSpan
        }

pOp :: LucretiaParser ExprSpan
pOp = addSrcSpan $ do
    opType <- pOpType
    args <- PT.parens lexer $ PT.commaSep lexer pExpr'
    return $ \srcSpan -> Op {
            opType = opType,
            args = args,
            exprAnnot = srcSpan
        }

pOpType :: LucretiaParser OpType
pOpType = do
        name <- PT.operator lexer
        case signToOp name of
            Nothing -> Parsec.parserFail $ "Undefined operator \"" ++ name ++ "\""
            Just op -> return op

pFun :: LucretiaParser ExprSpan
pFun = addSrcSpan $ do
    PT.reserved lexer "func"
    argVarIds <- PT.parens lexer $ PT.commaSep lexer pVarIdentAnn
    body <- PT.braces lexer pExpr
    return $ \srcSpan -> Fun {
            argVarIds = argVarIds,
            body = body,
            exprAnnot = srcSpan
        }

pAppl :: LucretiaParser ExprSpan
pAppl = addSrcSpan $ do
    let pApplFun = try pProp <|> pVar <|> pFun <|> PT.parens lexer pExpr
    fun <- pApplFun
    args <- PT.parens lexer $ PT.commaSep lexer pExpr'
    return $ \srcSpan -> Appl {
            fun = fun,
            args = args,
            exprAnnot = srcSpan
        }

pImport :: LucretiaParser ExprSpan
pImport = addSrcSpan $ do
    PT.reserved lexer "import"
    filePath <- PT.parens lexer $ PT.stringLiteral lexer
    return $ Import filePath

pNone :: LucretiaParser ExprSpan
pNone = addSrcSpan $ do
    PT.reserved lexer "None"
    return ConstNone

pBool :: LucretiaParser ExprSpan
pBool = addSrcSpan pTrue <|> addSrcSpan pFalse where
    pTrue = do
        PT.reserved lexer "True"
        return $ ConstBool True
    pFalse = do
        PT.reserved lexer "False"
        return $ ConstBool False

pSignPositive :: LucretiaParser Bool
pSignPositive = PT.lexeme lexer $
    (PC.char '-' >> return False) <|>
    (PC.char '+' >> return True) <|>
    return True

pNum :: LucretiaParser ExprSpan
pNum = addSrcSpan $ do
        signPositive <- pSignPositive
        let signFunction :: Num a => a -> a
            signFunction = if signPositive then id else negate
        numVal <- PT.naturalOrFloat lexer
        let pImag :: (forall a. Num a => a -> a) -> LucretiaParser (SrcSpan -> ExprSpan)
            pImag s = do
                PT.symbol lexer "j"
                case numVal of
                    Left intVal -> return $ ConstImaginary $ fromInteger $ s intVal
                    Right floatVal -> return $ ConstImaginary $ s floatVal
            pReal :: (forall a. Num a => a -> a) -> LucretiaParser (SrcSpan -> ExprSpan)
            pReal s =
                case numVal of
                    Left intVal -> return $ ConstInt $ s intVal
                    Right floatVal -> return $ ConstFloat $ s floatVal
        pImag signFunction <|> pReal signFunction

pString :: LucretiaParser ExprSpan
pString = addSrcSpan $ do
    strVal <- PT.stringLiteral lexer
    return $ ConstString strVal
