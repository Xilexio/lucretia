module Language.Lucretia.RuntimeError where

import Language.Lucretia.AST
import Control.Monad.Except
import Language.Python.Common.SrcLocation (SrcSpan (..))

data RuntimeError =
    VarNotInScope { errVarId :: VarIdent, srcSpan :: SrcSpan } |
    DotOnNonObject { errVarId :: VarIdent, srcSpan :: SrcSpan } |
    NonexistentPropReferenced { errObjProp :: ObjPropIdent, srcSpan :: SrcSpan } |
    InvalidOpArgCount { errOpType :: OpType, srcSpan :: SrcSpan } |
    InvalidFunArgCount { srcSpan :: SrcSpan } |
    ApplOnNonFun { srcSpan :: SrcSpan } |
    InvalidOpArg { errOpType :: OpType, srcSpan :: SrcSpan } |
    DivisionByZero { errOpType :: OpType, srcSpan :: SrcSpan } |
    NegativeShiftOffset { errOpType :: OpType, srcSpan :: SrcSpan } |
    ImportNotParsed { errFilePath :: String, srcSpan :: SrcSpan }
    deriving (Show, Eq)

data RuntimeErrorWithStackTrace = RuntimeErrorWithStackTrace RuntimeError [SrcSpan] deriving (Show, Eq)
