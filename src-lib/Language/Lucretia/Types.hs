module Language.Lucretia.Types where

import Data.Map (Map)


data Type =
    TInt |
    TBool |
    TFloat |
    TComplex |
    TString |
    TNone |
    TObject { tProps :: Map String Type } |
    TFun |
    TVar { tVarId :: TypeIdent } |
    TAnd { lArg :: Type, rArg :: Type } |
    TOr { lArg :: Type, rArg :: Type}

data Context = Context {
        types :: Map TypeIdent Type
    }

data TypeIdent = TypeIdent { typeIdentNum :: Int } deriving (Eq, Ord, Show)
