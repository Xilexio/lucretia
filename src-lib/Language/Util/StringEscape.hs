module Language.Util.StringEscape (
    escapeChar,
    escapeString
) where

-- http://docs.python.org/3.3/reference/lexical_analysis.html#string-and-bytes-literals
escapeChar :: Char -> String
escapeChar '\\' = "\\\\"
escapeChar '\'' = "\\'"
escapeChar '"' = "\\\""
escapeChar '\a' = "\\a"
escapeChar '\b' = "\\b"
escapeChar '\f' = "\\f"
escapeChar '\n' = "\\n"
escapeChar '\r' = "\\r"
escapeChar '\t' = "\\t"
escapeChar '\v' = "\\v"
escapeChar c = [c]

escapeString :: String -> String
escapeString "" = ""
escapeString (c:cs) = escapeChar c ++ escapeString cs
