module Language.Util.VarMap where

import Prelude hiding (null, lookup, filter)
import qualified Data.List as List
import Data.Hashable (Hashable)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Control.Lens
import Control.Monad.State
import Control.Applicative (pure)
import Data.Tuple (swap)
import Language.Util.Common (reverseMapping)
import Text.Parsec (ParsecT)
import qualified Text.Parsec as Parsec

data VarMap k v = VarMap {
        _vars :: HashMap k v,
        _ids :: HashMap v k,
        _nextId :: v
    } deriving (Show, Eq)
makeLenses ''VarMap
    
empty :: Enum v => VarMap k v
empty = VarMap { _vars = HashMap.empty, _ids = HashMap.empty, _nextId = toEnum 0 }

null :: VarMap k v -> Bool
null = HashMap.null . _ids

lookup :: (Hashable k, Eq k) => k -> VarMap k v -> Maybe v
lookup k = HashMap.lookup k . _vars

lookupI :: (Hashable v, Eq v) => v -> VarMap k v -> Maybe k
lookupI v = HashMap.lookup v . _ids

-- fresh var on state lens
freshVar :: (MonadState s m, Enum v) => Lens' s (VarMap k v) -> m v
freshVar ml = do
    nid <- use (ml . nextId)
    (ml . nextId) %= succ
    return nid

incrementFreshVar :: Enum v => VarMap k v -> (v, VarMap k v)
incrementFreshVar m = (m ^. nextId, m & nextId %~ succ)

parsecFreshVar :: (Monad m, Enum v) => Lens' u (VarMap k v) -> ParsecT s u m v
parsecFreshVar ml = do
    s <- Parsec.getState
    let (v, m') = incrementFreshVar (s ^. ml)
    Parsec.putState (s & ml .~ m')
    return v 
   
insertWithKey :: (Hashable k, Eq k, Hashable v, Eq v, Enum v) => k -> VarMap k v -> (v, VarMap k v)
insertWithKey k m = case lookup k m of
    Nothing -> flip runState m $ do
        nid <- use nextId
        nextId %= succ
        vars . at k ?= nid
        ids . at nid ?= k
        return nid
    Just v -> (v, m)

insert :: (Hashable k, Eq k, Hashable v, Eq v, Enum v) => k -> VarMap k v -> VarMap k v
insert k m = snd $ insertWithKey k m

delete :: (Hashable k, Eq k, Hashable v, Eq v) => k -> VarMap k v -> VarMap k v
delete k m = case lookup k m of
    Nothing -> m
    Just v -> flip execState m $ do
        vars . at k .= Nothing
        ids . at v .= Nothing

-- insert on lens
(<@+~) :: (Hashable k, Eq k, Hashable v, Eq v, Enum v) => Lens' s (VarMap k v) -> k -> s -> (v, s)
(<@+~) ml k s = (v, set ml m' s) where
    (v, m') = insertWithKey k $ view ml s

-- insert on state lens
(<@+=) :: (MonadState s m, Hashable k, Eq k, Hashable v, Eq v, Enum v) => Lens' s (VarMap k v) -> k -> m v
(<@+=) ml k = do
    m <- use ml
    let (v, m') = insertWithKey k m
    ml .= m'
    return v

merge :: (Hashable k, Eq k, Hashable v, Eq v, Enum v, Ord v) => VarMap k v -> VarMap k v -> (VarMap k v, v -> v)
merge a b = (a `union` b', conv) where
    conv = varsConversion $ collisionMap a b
    b' = mapVars conv b

mapVars :: (Hashable v, Eq v, Enum v, Ord v) => (v -> v) -> VarMap k v -> VarMap k v
mapVars conv = fromVarMap . HashMap.map conv . _vars
    
fromVarMap :: (Hashable v, Eq v, Enum v, Ord v) => HashMap k v -> VarMap k v
fromVarMap m = VarMap {
        _vars = m,
        _ids = reverseMapping m,
        _nextId = HashMap.foldl' (\a v -> max a (succ v)) (toEnum 0) m
    }
    
fromIdMap :: (Hashable k, Eq k, Enum v, Ord v) => HashMap v k -> VarMap k v
fromIdMap m = VarMap {
    _vars = reverseMapping m,
    _ids = m,
    _nextId = HashMap.foldlWithKey' (\a k _ -> max a (succ k)) (toEnum 0) m
}

varsConversion :: (Hashable v, Eq v) => HashMap v v -> v -> v
varsConversion m k = HashMap.lookupDefault k k m

-- union of two VarMaps, requires them to be compatibile (no clash in ids or vars) or the result is undefined
union :: (Hashable k, Eq k, Hashable v, Eq v, Ord v) => VarMap k v -> VarMap k v -> VarMap k v
union a b = flip execState a $ do
    vars %= HashMap.union (b ^. vars)
    ids %= HashMap.union (b ^. ids)
    nextId %= max (b ^. nextId)
    
compatibile :: (Hashable k, Eq k, Hashable v, Eq v, Enum v, Ord v) => VarMap k v -> VarMap k v -> Bool
compatibile a b = HashMap.null $ collisionMap a b
    
collisionMap :: (Hashable k, Eq k, Hashable v, Eq v, Enum v, Ord v) => VarMap k v -> VarMap k v -> HashMap v v
collisionMap a b = collisionMap' where
    -- two same names should have the same number (left stays same)
    collidingNamesMap = HashMap.fromList $ List.filter (uncurry (/=)) $ HashMap.elems $ HashMap.intersectionWith (,) (b ^. vars) (a ^. vars)
    -- when two different names have the same number, the second one should be re-assigned (left stays same)
    collidingBIds = HashMap.keys $ HashMap.filter (uncurry (/=)) $ HashMap.intersectionWith (,) (b ^. ids) (a ^. ids)
    prevNextId = max (a ^. nextId) (b ^. nextId)
    collidingIdsMapList = flip evalState prevNextId $ flip imapM collidingBIds $ \ix id -> do
        nid <- get
        modify succ
        return (id, nid)
    collidingIdsMap = HashMap.fromList collidingIdsMapList
    -- when we have both collisions, the first takes precedence (to map the same variable)
    collisionMap' = collidingNamesMap `HashMap.union` collidingIdsMap

filter :: (Hashable k, Eq k, Enum v, Ord v) => (k -> Bool) -> VarMap k v -> VarMap k v
filter f = fromIdMap . HashMap.filter f . _ids

filterI :: (Hashable v, Eq v, Enum v, Ord v) => (v -> Bool) -> VarMap k v -> VarMap k v
filterI f = fromVarMap . HashMap.filter f . _vars