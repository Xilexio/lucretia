module Language.Util.Common where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Tuple (swap)
import System.Exit (exitFailure)
import System.IO
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Hashable (Hashable)

class ReversibleMapping t k v where
    reverseMapping :: t k v -> t v k
    
instance (Ord v) => ReversibleMapping Map k v where
    reverseMapping = Map.fromList . map swap . Map.toList
    
instance (Hashable v, Eq v) => ReversibleMapping HashMap k v where
    reverseMapping = HashMap.fromList . map swap . HashMap.toList

tabSize :: Int
tabSize = 2

programVer = "0.1"

-- name of environment variable that defines library directory
lucLibPathEnvVar = "LUCRETIA_PATH"
-- default value of library directory if environment variable is not present,
-- relative to lucretia's executable's directory
defaultLucLibPath = "lib"

internalError msg = error $ "Internal error: " ++ msg

rightOrFail :: Either String t -> IO t
rightOrFail value = case value of
    Left err -> do
        hPutStrLn stderr err
        exitFailure
    Right value' -> return value'
