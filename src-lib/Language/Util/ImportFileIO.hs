module Language.Util.ImportFileIO where

import Language.Util.Common
import Control.Monad
import System.IO.Error
import System.FilePath

#if MIN_VERSION_base(4,6,0)
import System.Environment (lookupEnv, getExecutablePath)
#else
import System.Environment (getEnvironment)

lookupEnv :: String -> IO (Maybe String)
lookupEnv name = do
    env <- getEnvironment
    let foundEnv = filter (\(k,v) -> k == name) env
    case foundEnv of
        [] -> return Nothing
        [foundVal] -> return $ Just $ snd foundVal
        _ -> error $ "Internal error: Multiple environment variables \"" ++ name ++ "\" found."
#endif

getLibraryPath :: IO FilePath
getLibraryPath = do
    lucretiaPath <- lookupEnv lucLibPathEnvVar
    case lucretiaPath of
        Just path -> return path
        Nothing -> do
            exePath <-
#if MIN_VERSION_base(4,6,0)
                getExecutablePath
#else
                error $ lucLibPathEnvVar ++ " environment variable has to be set or Lucretia has to be compiled with base >=4.6.0.0."
#endif
            return $ takeDirectory exePath </> defaultLucLibPath

getRealFilePath :: FilePath -> IO FilePath
getRealFilePath lucFilePath = case lucFilePath of
    '%':'/':libFilePath -> do
        libRootPath <- getLibraryPath
        return $ libRootPath </> libFilePath
    _ -> return lucFilePath

readImportedFile :: FilePath -> IO (Either IOError String)
readImportedFile lucFilePath = do
    realFilePath <- getRealFilePath lucFilePath
    catchIOError (liftM Right $ readFile realFilePath) $ return . Left
