module Language.Util.ParserCommon where

import Language.Lucretia.AST
import Language.Python.Common.AST (Annotated (..))
import Language.Python.Common.SrcLocation (SrcSpan (..), combineSrcSpans)

buildVarId varId = VarIdentAnn varId SpanEmpty
buildVar varId = Var (buildVarId varId) SpanEmpty
buildObjProp objPropId = ObjPropIdentAnn objPropId SpanEmpty

fixSrcSpan :: ExprSpan -> ExprSpan
fixSrcSpan expr =
    let fixedChildrenExpr = case expr of
            LetIn _ binding body _ -> expr { binding = fixSrcSpan binding, body = fixSrcSpan body }
            AssignProp _ value _ -> expr { value = fixSrcSpan value }
            IfElse cond whenTrue whenFalse _ -> expr { cond = fixSrcSpan cond, whenTrue = fixSrcSpan whenTrue, whenFalse = fixSrcSpan whenFalse }
            IfHasAttr objExpr _ whenTrue whenFalse _ -> expr { objExpr = fixSrcSpan objExpr, whenTrue = fixSrcSpan whenTrue, whenFalse = fixSrcSpan whenFalse }
            Op _ args _ -> expr { args = map fixSrcSpan args }
            Fun _ body _ -> expr { body = fixSrcSpan body }
            Appl fun args _ -> expr { fun = fixSrcSpan fun, args = map fixSrcSpan args }
            _ -> expr in
    let combineSrcSpanList = foldl combineSrcSpans SpanEmpty in
    case expr of
        LetIn varId binding body exprAnnot ->
            expr { exprAnnot = combineSrcSpanList [exprAnnot, annot varId, annot binding, annot body] }
        Var varId exprAnnot ->
            expr { exprAnnot = combineSrcSpanList [exprAnnot, annot varId] }
        Prop objProp exprAnnot ->
            expr { exprAnnot = combineSrcSpanList [exprAnnot, annot objProp] }
        AssignProp objProp value exprAnnot ->
            expr { exprAnnot = combineSrcSpanList [exprAnnot, annot objProp, annot value] }
        IfElse cond whenTrue whenFalse exprAnnot ->
            expr { exprAnnot = combineSrcSpanList [exprAnnot, annot cond, annot whenTrue, annot whenFalse] }
        IfHasAttr objExpr _ whenTrue whenFalse exprAnnot ->
            expr { exprAnnot = combineSrcSpanList [exprAnnot, annot objExpr, annot whenTrue, annot whenFalse] }
        Op _ args exprAnnot ->
            expr { exprAnnot = combineSrcSpanList (exprAnnot:map annot args) }
        Fun argVarIds body exprAnnot ->
            expr { exprAnnot = combineSrcSpanList (exprAnnot:annot body:map annot argVarIds) }
        Appl fun args exprAnnot ->
            expr { exprAnnot = combineSrcSpanList (exprAnnot:annot fun:map annot args) }
        _ -> expr
