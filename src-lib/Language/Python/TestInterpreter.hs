module Language.Python.TestInterpreter where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import Language.Lucretia.Interpreter
import Language.Python.Common.SrcLocation (SrcSpan (..))
import Language.Pretty.Common
import Language.Pretty.RuntimeError
import Language.Python.Converter
import Language.Lucretia.RunEnvironment
import System.FilePath
import System.IO

python3ModuleTestExpr :: FileExprsWithMetaDataSpan -> FileExprWithMetaDataSpan -> Either String (ExprSpan, Map.Map PropIdent String)
python3ModuleTestExpr builtins fileExpr =
    -- synchronizing propIds
    let fileExprs = addFileExpr fileExpr builtins
        fileExpr' = fromJust $ getFileExpr (fPath fileExpr) fileExprs in
    case Map.lookup "test" $ fPropIds fileExpr' of
        Nothing -> Left "test variable not found"
        Just testPropId ->
            -- getting "test" value of a module (with arbitrary VarIdent - not SSA)
            let moduleVarId = VarIdent 0 in
            Right (LetIn {
                varId = VarIdentAnn moduleVarId SpanEmpty,
                binding = fExpr fileExpr',
                body = Prop {
                    objProp = ObjPropIdentAnn {
                        rawObjProp = ObjPropIdent {
                                objVarId = moduleVarId,
                                objPropId = testPropId
                            },
                        objPropAnnot = SpanEmpty
                    },
                    exprAnnot = SpanEmpty
                },
                exprAnnot = SpanEmpty
            }, fAllPropNames fileExprs)

python3FileTestExpr :: FileExprsWithMetaDataSpan -> FilePath -> IO (Either String (ExprSpan, Map.Map PropIdent String))
python3FileTestExpr builtins fileName = do
    parseResult <- parsePython3File fileName
    case parseResult of
        Left err -> return $ Left $ "Conversion error: " ++ err
        Right fileExpr -> do
            let testExpr = python3ModuleTestExpr builtins fileExpr
            case testExpr of
                Left err -> return $ Left $ "Python test construction error: " ++ err
                Right _ -> return testExpr

evalPython3TestExpr :: FileExprsWithMetaDataSpan -> ExprSpan -> IO (Either String (Value, RunEnvState))
evalPython3TestExpr builtins testExpr =
    case eval builtins testExpr of
        Left err -> return $ Left $ "Runtime error: " ++ prettyShow Map.empty (fAllPropNames builtins) err
        Right (value, state) -> return $ Right (value, state)
