module Language.Python.FileConverter (
    pythonFileConverter
) where

import Language.Python.ConvertEnvironment
import Language.Pretty.AST
import Language.Python.Converter
import System.Exit (exitFailure)
import System.IO

pythonFileConverter :: String -> IO ()
pythonFileConverter filePath = do
    parseResult <- parsePython3File filePath
    case parseResult of
        Left err -> do
            hPutStrLn stderr err
            exitFailure
        Right fileExpr ->
            putStrLn $ prettyShowFileExpr fileExpr
