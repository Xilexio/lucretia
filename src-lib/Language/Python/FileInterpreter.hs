module Language.Python.FileInterpreter where

import Language.Util.Common
import Language.Python.ConvertEnvironment
import Language.Pretty.Value
import Language.Python.Converter
import Language.Python.TestInterpreter
import System.IO

pythonFileInterpreter :: String -> IO ()
pythonFileInterpreter filePath = do
    builtins <- rightOrFail =<< parsePythonBuiltins
    (testExpr, propNames) <- rightOrFail =<< python3FileTestExpr builtins filePath
    (value, state) <- rightOrFail =<< evalPython3TestExpr builtins testExpr
    putStrLn $ showValue state 1 propNames value ++ " : " ++ showType value
