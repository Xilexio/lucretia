module Language.Python.ConvertEnvironment where

import Language.Lucretia.AST
import Language.Util.Common (internalError)
import Control.Monad.State
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import Data.List (find)

data ScopeType = ModuleScope | FunctionScope | WhileScope | ClassScope deriving (Eq, Show)
data Scope = Scope { scopeIdent :: VarIdent, scopeType :: ScopeType, vars :: Map.Map String PropIdent }

data VarEnvState = VarEnvState { nextIdentNum :: Int, scopes :: [Scope], props :: Map.Map String PropIdent }
emptyVarEnvState :: VarEnvState
emptyVarEnvState = VarEnvState {
        nextIdentNum = 1,
        scopes = [],
        props = Map.empty
    }

-- creates unnamed variable
newIdentNum :: MonadState VarEnvState m => m Int
newIdentNum = do
    newIdentNum <- gets nextIdentNum
    modify $ \s -> s { nextIdentNum = newIdentNum + 1 }
    return newIdentNum

createUnnamedVarIdent :: MonadState VarEnvState m => m VarIdent
createUnnamedVarIdent = liftM VarIdent newIdentNum

createUnnamedPropIdent :: MonadState VarEnvState m => m PropIdent
createUnnamedPropIdent = liftM PropIdent newIdentNum

pushScopeObject :: MonadState VarEnvState m => ScopeType -> m VarIdent
pushScopeObject scopeType = do
    scopeIdent <- createUnnamedVarIdent
    let newScope = Scope { scopeIdent = scopeIdent, scopeType = scopeType, vars = Map.empty }
    modify $ \s -> s { scopes = newScope : scopes s }
    return scopeIdent

popScopeObject :: MonadState VarEnvState m => m ()
popScopeObject = modify $ \s -> s { scopes = tail $ scopes s }

currentModule :: MonadState VarEnvState m => m VarIdent
currentModule = do
    scopes' <- gets scopes
    return $ scopeIdent $ fromJust $ find isModuleScope scopes'

-- namespace scope type is one that has its own local variables in Python
-- other types of scopes are added internally for converter
isNamespace :: Scope -> Bool
isNamespace s = scopeType s /= WhileScope

-- whether this namespace variables can be accessed from its subscopes
isAccessibleAsNonlocal :: Scope -> Bool
isAccessibleAsNonlocal s = isNamespace s && scopeType s /= ClassScope

isModuleScope :: Scope -> Bool
isModuleScope s = scopeType s == ModuleScope

currentNamespaceType :: MonadState VarEnvState m => m ScopeType
currentNamespaceType = do
    scopes' <- gets scopes
    return $ scopeType $ fromJust $ find isNamespace scopes'

currentScopeType :: MonadState VarEnvState m => m ScopeType
currentScopeType = do
    scopes' <- gets scopes
    return $ scopeType $ head scopes'

createInternalVar :: MonadState VarEnvState m => String -> m ObjPropIdent
createInternalVar name = do
    currentScope:lowerScopes <- gets scopes
    let scopeIdent' = scopeIdent currentScope
    case Map.lookup name $ vars currentScope of
        Nothing -> do
            newPropId <- lookupOrCreatePropId name
            let updatedVars = Map.insert name newPropId $ vars currentScope
            let updatedScopes = (currentScope { vars = updatedVars}):lowerScopes
            modify $ \s -> s { scopes = updatedScopes }
            return $ ObjPropIdent scopeIdent' newPropId
        Just propId -> internalError "internal variable already exists"

createUnnamedInternalVar :: MonadState VarEnvState m => m ObjPropIdent
createUnnamedInternalVar = do
    scopes' <- gets scopes
    let scopeIdent' = scopeIdent $ head scopes'
    newPropId <- createUnnamedPropIdent
    return $ ObjPropIdent scopeIdent' newPropId

-- adds or updates new variable in current scope (doesn't depend on lower scopes)
-- instead of having reserved identifiers, one can create a new variable with invalid name (for example #x)
lookupOrCreateVar :: MonadState VarEnvState m => String -> m ObjPropIdent
lookupOrCreateVar name = do
    scopes' <- gets scopes
    let (nonNamespaces, currentNamespace:lowerScopes) = break isNamespace scopes'
    let scopeIdent' = scopeIdent currentNamespace
    case Map.lookup name $ vars currentNamespace of
        Nothing -> do
            newPropId <- lookupOrCreatePropId name
            let updatedVars = Map.insert name newPropId $ vars currentNamespace
            let updatedScopes = nonNamespaces ++ (currentNamespace { vars = updatedVars}):lowerScopes
            modify $ \s -> s { scopes = updatedScopes }
            return $ ObjPropIdent scopeIdent' newPropId
        Just propId -> return $ ObjPropIdent scopeIdent' propId

-- looks up a variable from current scope to highest module scope
-- http://docs.python.org/3.3/tutorial/classes.html#python-scopes-and-namespaces
-- http://docs.python.org/3/reference/simple_stmts.html#the-global-statement
-- http://docs.python.org/3/reference/simple_stmts.html#the-nonlocal-statement
data LookupVarResult = NotFound |
                       Local { resultObjPropId :: ObjPropIdent } |
                       NonLocal { resultObjPropId :: ObjPropIdent } |
                       Global { resultObjPropId :: ObjPropIdent }

lookupVar :: MonadState VarEnvState m => String -> m LookupVarResult
lookupVar name  = do
    scopes' <- gets scopes
    let (localNamespace:nonlocalNamespaces) = filter isNamespace scopes'
    let accessibleNamespacesInAllModules = localNamespace : filter isAccessibleAsNonlocal nonlocalNamespaces
    let (accessibleNamespacesInCurrentModule, currentModuleScope:_) = break isModuleScope accessibleNamespacesInAllModules
    let accessibleNamespaces = accessibleNamespacesInCurrentModule ++ [currentModuleScope]
    return $ findVar True accessibleNamespaces where
        findVar _ [] = NotFound
        findVar isTopScope (currentScope:lowerScopes) =
            case Map.lookup name $ vars currentScope of
                Nothing -> findVar False lowerScopes
                Just propId ->
                    if isTopScope then
                        Local $ ObjPropIdent (scopeIdent currentScope) propId
                    else case scopeType currentScope of
                        ModuleScope -> Global $ ObjPropIdent (scopeIdent currentScope) propId
                        FunctionScope -> NonLocal $ ObjPropIdent (scopeIdent currentScope) propId

-- like lookupVar, but ignores accessibility as nonlocal variable; assumes existence of the variable
lookupInternalVar :: MonadState VarEnvState m => String -> m ObjPropIdent
lookupInternalVar name = do
    scopes' <- gets scopes
    return $ findVar scopes' where
        findVar [] = internalError "internal variable not found"
        findVar (currentScope:lowerScopes) =
            case Map.lookup name $ vars currentScope of
                Nothing -> findVar lowerScopes
                Just propId -> ObjPropIdent (scopeIdent currentScope) propId

-- looks up a property and returns its id, creating one if it didn't exist before
lookupOrCreatePropId :: MonadState VarEnvState m => String -> m PropIdent
lookupOrCreatePropId name = do
    props' <- gets props
    let propId = Map.lookup name props'
    case propId of
        Nothing -> do
            newIdent <- createUnnamedPropIdent
            let updatedProps = Map.insert name newIdent props'
            modify $ \s -> s { props = updatedProps }
            return newIdent
        Just id -> return id

-- looks up a property and returns its id; assumes it existed before
lookupInternalPropId :: MonadState VarEnvState m => String -> m PropIdent
lookupInternalPropId name = do
    props' <- gets props
    let propId = Map.lookup name props'
    case propId of
        Nothing -> internalError "internal property not found"
        Just id -> return id
