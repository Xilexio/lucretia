module Language.Python.Converter (
    parsePython3,
    parsePython3File,
    parsePythonBuiltins
) where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions
import Language.Util.ParserCommon
import Language.Util.Common
import Control.Monad.Except
import Control.Monad.State
import Control.Monad.Identity (Identity, runIdentity)
import qualified Data.Map as Map
import qualified Data.Set as Set
import Development.Placeholders
import Language.Python.ConvertEnvironment
import Language.Python.Common.AST (Annotated (..))
import qualified Language.Python.Common as Python
import qualified Language.Python.Common.StringEscape as PythonStr
import Language.Python.Common.SrcLocation (SrcSpan (..), combineSrcSpans)
import qualified Language.Python.Version2 as Python2
import qualified Language.Python.Version3 as Python3
import qualified Language.Lucretia.Parser as LucP
import qualified System.IO

pythonBuiltinsPaths :: Set.Set String
pythonBuiltinsPaths = Set.fromList ["%/builtins.luc"]

parsePythonBuiltins :: IO (Either String FileExprsWithMetaDataSpan)
parsePythonBuiltins = LucP.recursivelyParseLucretiaFiles pythonBuiltinsPaths emptyFileExprsWithMetaData

parsePython3File :: String -> IO (Either String FileExprWithMetaDataSpan)
parsePython3File filePath = do
    contents <- readFile filePath
    return $ parsePython3 filePath contents

parsePython3 :: String -> String -> Either String FileExprWithMetaDataSpan
parsePython3 = parsePython Python3.parseModule

parsePython moduleParser sourceName contents =
    -- \n being a workaround for a bug/feature in language-python parser that expects \n at the end of the file
    let parseOutput = moduleParser (contents ++ "\n") sourceName in
    case parseOutput of
        Left parseError -> Left $ show parseError
        Right (pyModule, _) -> pythonModuleToLucretiaExpr sourceName pyModule

-- parser environment
type VarEnv = StateT VarEnvState (ExceptT String Identity)

-- wrapper for executing parsing
pythonModuleToLucretiaExpr :: String -> Python.ModuleSpan -> Either String FileExprWithMetaDataSpan
pythonModuleToLucretiaExpr sourceName pyModule =
    case runIdentity $ runExceptT $ runStateT (convertModule pyModule) emptyVarEnvState of
        Left err -> Left err
        Right (expr, varEnv) -> Right FileExprWithMetaData {
                fExpr = expr,
                fPropNames = reverseMapping $ props varEnv,
                fPropIds = props varEnv,
                fPath = sourceName
            }

-- notation: py* (i.e. pyExpr) for variables with Python grammar elements

------- helper functions -------
-- statements are converted into Expr -> Expr, which mean \followingExpr -> let result = expr in followingExpr
-- unless it's a definition of a local variable, results are ignored
-- let's call them expression segments
-- such sequences implement bind operator (;) in a monadic way

joinExprSegments :: [ExprSpan -> ExprSpan] -> ExprSpan -> ExprSpan
joinExprSegments [] finalExpr = finalExpr
joinExprSegments (exprSegment : exprSegments) finalExpr = foldl (.) exprSegment exprSegments finalExpr

-- sequencing operators
-- >>=
stepWithBindAnn :: SrcSpan -> VarIdent -> ExprSpan -> VarEnv (ExprSpan -> ExprSpan)
stepWithBindAnn annot varId first = return $ \second -> LetIn {
        varId = VarIdentAnn varId SpanEmpty,
        binding = first,
        body = second,
        exprAnnot = annot
    }

stepWithBind = stepWithBindAnn SpanEmpty

-- >>
stepWithoutBindAnn :: SrcSpan -> ExprSpan -> VarEnv (ExprSpan -> ExprSpan)
stepWithoutBindAnn annot first = do
    dummyVar <- createUnnamedVarIdent
    stepWithBindAnn annot dummyVar first

stepWithoutBind = stepWithoutBindAnn SpanEmpty

-- types of interruption of a loop
noInterruption = ConstInt 0 SpanEmpty
breakInterruption = ConstInt 1 SpanEmpty
continueInterruption = ConstInt 2 SpanEmpty

-- decorating expressions before which change of control flow might occur (i.e. after return)
maybeSkip :: VarEnv (ExprSpan -> ExprSpan)
maybeSkip = do
    mainScopeType' <- currentNamespaceType
    returnSkip <- case mainScopeType' of
        FunctionScope -> do
            returnedVar <- lookupInternalVar "__luc_returned"
            let cond = Op Equal [
                        Prop {
                            objProp = buildObjProp returnedVar,
                            exprAnnot = SpanEmpty
                        },
                        ConstBool False SpanEmpty
                    ] SpanEmpty
            return $ \expr -> IfElse {
                    cond = cond,
                    whenTrue = expr,
                    whenFalse = ConstNone SpanEmpty,
                    exprAnnot = annot expr
                }
        _ -> return id
    scopeType' <- currentScopeType
    loopInterruptionSkip <- case scopeType' of
        WhileScope -> do
            interruptionVar <- lookupInternalVar "__luc_loopInterruption"
            let cond = Op Equal [
                        Prop {
                            objProp = buildObjProp interruptionVar,
                            exprAnnot = SpanEmpty
                        },
                        noInterruption
                    ] SpanEmpty
            return $ \expr -> IfElse {
                    cond = cond,
                    whenTrue = expr,
                    whenFalse = ConstNone SpanEmpty,
                    exprAnnot = annot expr
                }
        _ -> return id
    return $ returnSkip . loopInterruptionSkip

-- http://docs.python.org/2/reference/lexical_analysis.html#string-literals
-- http://docs.python.org/3.3/reference/lexical_analysis.html#string-and-bytes-literals
-- TODO there are few bugs in Language.Python.Common.StringEscape about it
-- TODO in Python 3.3 r modifier might be before the other one
toRawString :: String -> String
toRawString str =
    if length str < 2 then
        internalError "string too short"
    else
        case str of
            'u':str' -> toRawString str'
            'U':str' -> toRawString str'
            'b':str' -> toRawString str'
            'B':str' -> toRawString str'
            'r':str' -> PythonStr.unescapeRawString $ removeQuotes str'
            'R':str' -> PythonStr.unescapeRawString $ removeQuotes str'
            '\'':_ -> PythonStr.unescapeString $ removeQuotes str
            '"':_ -> PythonStr.unescapeString $ removeQuotes str
            _ -> internalError "invalid string prefix"
    where
        removeQuotes ('\'':'\'':'\'':str') = case splitAt (length str' - 3) str' of
            (str'', "'''") -> str''
            _ -> internalError "invalid string quotes"
        removeQuotes ('"':'"':'"':str') = case splitAt (length str' - 3) str' of
            (str'', "\"\"\"") -> str''
            _ -> internalError "invalid string quotes"
        removeQuotes ('\'':str') = case splitAt (length str' - 1) str' of
            (str'', "'") -> str''
            _ -> internalError "invalid string quotes"
        removeQuotes ('"':str') = case splitAt (length str' - 1) str' of
            (str'', "\"") -> str''
            _ -> internalError "invalid string quotes"
        removeQuotes _ = internalError "invalid string quotes"

templateCodeShort :: String -> String
templateCodeShort code = take 12 code ++ "..." ++ reverse (take 12 $ reverse code)

-- parses Lucretia code in Python context - with its PropIdents, current module var as "module" VarIdent
-- arguments are additional optional variables and changed PropIdents
-- changing PropIdents can be used to create a "template" for any label
parseLucretiaIExprTemplate :: Map.Map String VarIdent -> Map.Map String PropIdent -> String -> VarEnv IExprSpan
parseLucretiaIExprTemplate vars modifiedProps code = do
    nextIdentNum' <- gets nextIdentNum
    props' <- gets props
    let props'' = Map.union modifiedProps props'
    currentModuleVar <- currentModule
    let vars' = Map.insert "module" currentModuleVar vars
    let startLucEnvState = LucP.LucEnvState {
            LucP.nextIdentNum = nextIdentNum',
            LucP.varIds = vars',
            LucP.propIds = props''
        }
    let parseResult = LucP.parseInterpretedLucretia "template" code startLucEnvState
    case parseResult of
        Left err -> internalError $ "Lucretia template \"" ++ templateCodeShort code ++ "...\" parse error: " ++ err
        Right (iExpr', lucEnvState) -> do
            let newProps = LucP.propIds lucEnvState
            let revertProp = Map.updateWithKey (\propName _ -> Map.lookup propName props')
            let newProps' = foldl (flip revertProp) newProps (Map.keys modifiedProps)
            modify $ \s -> s { nextIdentNum = LucP.nextIdentNum lucEnvState, props = newProps' }
            return iExpr'

parseLucretiaExprTemplate :: Map.Map String VarIdent -> Map.Map String PropIdent -> String -> VarEnv ExprSpan
parseLucretiaExprTemplate vars modifiedProps code = do
    iExpr' <- parseLucretiaIExprTemplate vars modifiedProps code
    case iExpr' of
        LucretiaExpr iExpr'' -> return iExpr''
        _ -> internalError $ "unexpected Lucretia expression segment in expression template \"" ++ templateCodeShort code ++ "...\""

parseLucretiaExprSegmentTemplate :: Map.Map String VarIdent -> Map.Map String PropIdent -> String -> VarEnv (ExprSpan -> ExprSpan)
parseLucretiaExprSegmentTemplate vars modifiedProps code = do
    iExpr' <- parseLucretiaIExprTemplate vars modifiedProps code
    case iExpr' of
        LucretiaExpr iExpr'' -> stepWithoutBind iExpr''
        LetSegment iVarId iBinding ->
            if elem (rawVarId iVarId) $ Map.elems vars then
                stepWithBind (rawVarId iVarId) iBinding
            else
                internalError $ "Lucretia expression segment template \"" ++ templateCodeShort code ++ "...\" used with fresh variable"

------- actual parser functions -------
---- top level in module ----
convertModule :: Python.ModuleSpan -> VarEnv ExprSpan
convertModule (Python.Module pySuite) = do
    -- prologue
    moduleScope <- pushScopeObject ModuleScope
    modulePrefix <- stepWithBind moduleScope $ New SpanEmpty
    builtinsVar <- lookupOrCreateVar "__builtins__"
    builtinsImport <- stepWithoutBind AssignProp {
            objProp = buildObjProp builtinsVar,
            value = Import {
                    filePath = "%/builtins.luc",
                    exprAnnot = SpanEmpty
                },
            exprAnnot = SpanEmpty
        }
    -- converting actual code
    moduleExprSegments <- convertSuite pySuite
    -- putting everything together and epilogue
    popScopeObject
    return $ fixSrcSpan $ modulePrefix . builtinsImport . moduleExprSegments $ buildVar moduleScope

---- statements ----
convertSuite :: Python.SuiteSpan -> VarEnv (ExprSpan -> ExprSpan)
convertSuite pySuite = do
    exprSegments <- mapM convertStmt pySuite
    return $ joinExprSegments exprSegments

-- converts a single Python statement into a lucretia expression segment
convertStmt :: Python.StatementSpan -> VarEnv (ExprSpan -> ExprSpan)

convertStmt (Python.Pass srcSpan) = stepWithoutBind $ ConstNone srcSpan

convertStmt (Python.Conditional pyGuards pyElseSuite srcSpan) = do
    let convertGuard (condExpr, bodySuite) = do
        cond <- convertExpr condExpr
        body <- convertSuite bodySuite
        maybeSkipSegment <- maybeSkip
        return $ maybeSkipSegment . \elseExpr -> IfElse {
                cond = cond,
                whenTrue = body $ ConstNone SpanEmpty,
                whenFalse = elseExpr,
                exprAnnot = SpanEmpty
            }
    guardsExprs <- mapM convertGuard pyGuards
    elseExpr <- convertSuite pyElseSuite
    stepWithoutBindAnn srcSpan $ joinExprSegments guardsExprs $ elseExpr $ ConstNone SpanEmpty

-- evaluation order: rhs, then lhs from left to right
-- http://docs.python.org/2/reference/simple_stmts.html#assignment-statements
convertStmt (Python.Assign pyLhsExprs pyRhsExpr srcSpan) = do
    rhsExpr <- convertExpr pyRhsExpr
    rhsVar <- createUnnamedVarIdent
    let createAssign (lhsPreparation, lhsId) = do
        assignExpr <- stepWithoutBind AssignProp {
                objProp = lhsId,
                value = buildVar rhsVar,
                exprAnnot = SpanEmpty
            }
        return $ lhsPreparation . assignExpr
    lhsAssignSegments <- mapM (convertAssignLhsExpr >=> createAssign) pyLhsExprs
    let joinedLhsExpr = joinExprSegments lhsAssignSegments $ ConstNone SpanEmpty
    maybeSkipSegment <- maybeSkip
    stepWithoutBindAnn srcSpan $ maybeSkipSegment LetIn {
            varId = VarIdentAnn rhsVar SpanEmpty,
            binding = rhsExpr,
            body = joinedLhsExpr,
            exprAnnot = SpanEmpty
        }

convertStmt (Python.AugmentedAssign _ _ _ _) = $notImplemented
convertStmt (Python.Delete _ _) = $notImplemented
convertStmt (Python.Print _ _ _ _) = $notImplemented
convertStmt (Python.With _ _ _) = $notImplemented

convertStmt (Python.StmtExpr pyExpr srcSpan) = convertExpr pyExpr >>= stepWithoutBindAnn srcSpan

convertStmt (Python.Exec _ _ _) = throwError "exec statement not supported"

-- http://docs.python.org/2/reference/compound_stmts.html#the-while-statement
convertStmt (Python.While pyCond pyBody pyElse srcSpan) = do
    whileId <- createUnnamedInternalVar
    let whileIdAnn = buildObjProp whileId
    scope <- pushScopeObject WhileScope
    interruptionVar <- createInternalVar "__luc_loopInterruption"
    let interruptionVarAnn = buildObjProp interruptionVar
    cond <- convertExpr pyCond
    body <- convertSuite pyBody
    popScopeObject
    p1 <- stepWithBind scope $ New SpanEmpty
    p2 <- stepWithoutBind AssignProp {
            objProp = interruptionVarAnn,
            value = noInterruption,
            exprAnnot = SpanEmpty
        }
    resetContinueInterruption <- stepWithoutBind IfElse {
            cond = Op Equal [Prop interruptionVarAnn SpanEmpty, continueInterruption] SpanEmpty,
            whenTrue = AssignProp {
                    objProp = interruptionVarAnn,
                    value = noInterruption,
                    exprAnnot = SpanEmpty
                },
            whenFalse = ConstNone SpanEmpty,
            exprAnnot = SpanEmpty
        }
    let epilogue = IfElse {
            cond = Op Equal [Prop interruptionVarAnn SpanEmpty, breakInterruption] SpanEmpty,
            whenTrue = ConstBool False SpanEmpty,
            whenFalse = resetContinueInterruption Appl {
                    fun = Prop {
                        objProp = whileIdAnn,
                        exprAnnot = SpanEmpty
                    },
                    args = [],
                    exprAnnot = SpanEmpty
                },
            exprAnnot = SpanEmpty
        }
    maybeSkipSegment1 <- maybeSkip
    let whileFunBody = maybeSkipSegment1 IfElse {
            cond = cond,
            whenTrue = p1 . p2 . body $ epilogue,
            whenFalse = ConstBool True SpanEmpty,
            exprAnnot = SpanEmpty
        }
    whileFunDef <- stepWithoutBind AssignProp {
            objProp = whileIdAnn,
            value = Fun {
                argVarIds = [],
                body = whileFunBody,
                exprAnnot = SpanEmpty
            },
            exprAnnot = SpanEmpty
        }
    whileResultVar <- createUnnamedVarIdent
    whileFunEval <- stepWithBind whileResultVar Appl {
            fun = Prop { objProp = whileIdAnn, exprAnnot = SpanEmpty },
            args = [],
            exprAnnot = SpanEmpty
        }
    rawElseExpr <- convertSuite pyElse
    maybeSkipSegment2 <- maybeSkip
    elseSegment <- stepWithoutBind $ maybeSkipSegment2 IfElse {
            cond = buildVar whileResultVar,
            whenTrue = rawElseExpr $ ConstNone SpanEmpty,
            whenFalse = ConstNone SpanEmpty,
            exprAnnot = SpanEmpty
        }
    stepWithoutBindAnn srcSpan $ whileFunDef . whileFunEval . elseSegment $ ConstNone SpanEmpty

convertStmt (Python.For _ _ _ _ _) = $notImplemented

convertStmt (Python.Break srcSpan) = do
    currentScopeType' <- currentScopeType
    unless (currentScopeType' == WhileScope) $ throwError "break statement used outside of a loop"
    interruptionVar <- lookupInternalVar "__luc_loopInterruption"
    maybeSkipSegment <- maybeSkip
    stepWithoutBindAnn srcSpan $ maybeSkipSegment AssignProp {
            objProp = buildObjProp interruptionVar,
            value = breakInterruption,
            exprAnnot = SpanEmpty
        }

convertStmt (Python.Continue srcSpan) = do
    currentScopeType' <- currentScopeType
    unless (currentScopeType' == WhileScope) $ throwError "continue statement used outside of a loop"
    interruptionVar <- lookupInternalVar "__luc_loopInterruption"
    maybeSkipSegment <- maybeSkip
    stepWithoutBindAnn srcSpan $ maybeSkipSegment AssignProp {
            objProp = buildObjProp interruptionVar,
            value = continueInterruption,
            exprAnnot = SpanEmpty
        }


convertStmt (Python.Fun pyIdent pyArgs _ pyBodySuite srcSpan) = do
    funId <- lookupOrCreateVar $ Python.ident_string pyIdent
    scope <- pushScopeObject FunctionScope
    createInternalVar "__luc_returned"
    createInternalVar "__luc_returnedValue"
    prologue1 <- stepWithBind scope $ New SpanEmpty
    prologue2 <- parseLucretiaExprSegmentTemplate
        (Map.singleton "scope" scope)
        Map.empty $
        unlines [
                "scope.__luc_returned = False;",
                "scope.__luc_returnedValue = None"
            ]
    argsVar <- createUnnamedVarIdent
    let createArgSegment arg = do
        argLocalId <- lookupOrCreateVar $ Python.ident_string $ Python.param_name arg
        parseLucretiaExprSegmentTemplate
            (Map.fromList [("scope", objVarId argLocalId), ("args", argsVar)])
            (Map.singleton "arg" $ objPropId argLocalId) $
            unlines [
                "let builtins = module.__builtins__;",
                "if builtins.__luc_hasArg(args) then",
                "  scope.arg = builtins.__luc_takeFirstArg(args)",
                "else",
                "  None"
            ]
    args <- liftM joinExprSegments $ mapM createArgSegment pyArgs
    body <- convertSuite pyBodySuite
    epilogue <- parseLucretiaExprTemplate
        (Map.singleton "scope" scope)
        Map.empty
        "scope.__luc_returnedValue"
    popScopeObject
    funVar <- createUnnamedVarIdent
    funDef <- stepWithBind funVar Fun {
            argVarIds = [buildVarId argsVar],
            body = prologue1 . prologue2 . args . body $ epilogue,
            exprAnnot = SpanEmpty
        }
    mkFunction <- parseLucretiaExprTemplate
        (Map.singleton "fun" funVar)
        Map.empty
        "module.__builtins__.__luc_mkFunction(new, fun)"
    maybeSkipSegment <- maybeSkip
    stepWithoutBind $ maybeSkipSegment AssignProp {
            objProp = buildObjProp funId,
            value = funDef mkFunction,
            exprAnnot = srcSpan
        }


convertStmt (Python.Return pyExpr srcSpan) = do
    expr <- case pyExpr of
        Nothing -> return $ ConstNone SpanEmpty
        Just pyExpr' -> convertExpr pyExpr'
    returnedVar <- lookupInternalVar "__luc_returned"
    returnedValueVar <- lookupInternalVar "__luc_returnedValue"
    r1 <- stepWithoutBind AssignProp {
            objProp = buildObjProp returnedVar,
            value = ConstBool True SpanEmpty,
            exprAnnot = SpanEmpty
        }
    r2 <- stepWithoutBind AssignProp {
            objProp = buildObjProp returnedValueVar,
            value = expr,
            exprAnnot = SpanEmpty
        }
    maybeSkipSegment <- maybeSkip
    stepWithoutBindAnn srcSpan $ maybeSkipSegment . r1 . r2 $ ConstNone SpanEmpty

convertStmt (Python.Decorated _ _ _) = $notImplemented


convertStmt (Python.Global _ _) = $notImplemented
convertStmt (Python.NonLocal _ _) = $notImplemented


convertStmt (Python.Class pyIdent pyArgs pySuite srcSpan) = do
    classId <- lookupOrCreateVar $ Python.ident_string pyIdent
    classScope <- pushScopeObject ClassScope
    classScopeCreation <- stepWithBind classScope $ New SpanEmpty

    -- initializing a class
    mkClass <- parseLucretiaExprSegmentTemplate
        (Map.singleton "class" classScope)
        Map.empty
        "module.__builtins__.__luc_mkClass(class)"

    -- processing base class expressions and appending them to __bases__
    -- TODO in Python 2 only ArgExpr, in Python 3 *args is okay
    let appendBase pyArg = case pyArg of
            Python.ArgExpr pyArgExpr _ -> do
                baseVar <- createUnnamedVarIdent
                baseExprSegment <- stepWithBind baseVar =<< convertExpr pyArgExpr
                appendingBase <- parseLucretiaExprSegmentTemplate
                    (Map.fromList [("class", classScope), ("baseClass", baseVar)])
                    Map.empty
                    "module.__builtins__.tuple.__luc_append(class.__bases__, baseClass)"
                return $ baseExprSegment . appendingBase
            _ -> $notImplemented -- Python 3 support
    fillingBases <- case pyArgs of
        [] ->
            parseLucretiaExprSegmentTemplate
                (Map.singleton "class" classScope)
                Map.empty
                "module.__builtins__.tuple.__luc_append(class.__bases__, module.__builtins__.object)"
        _ -> liftM joinExprSegments $ mapM appendBase pyArgs

    let classCreation = classScopeCreation . mkClass . fillingBases

    classSuite <- convertSuite pySuite
    popScopeObject
    let classDef = classCreation . classSuite $ buildVar classScope
    maybeSkipSegment <- maybeSkip
    stepWithoutBind $ maybeSkipSegment AssignProp {
            objProp = buildObjProp classId,
            value = classDef,
            exprAnnot = srcSpan
        }


convertStmt (Python.Try _ _ _ _ _) = $notImplemented
convertStmt (Python.Raise _ _) = $notImplemented
convertStmt (Python.Assert _ _) = $notImplemented


convertStmt (Python.Import _ _) = $notImplemented
convertStmt (Python.FromImport _ _ _) = $notImplemented

---- expressions ----
convertExpr :: Python.ExprSpan -> VarEnv ExprSpan

convertExpr (Python.Int value _ srcSpan) = return $ ConstInt value srcSpan
convertExpr (Python.LongInt value _ srcSpan) = return $ ConstInt value srcSpan
convertExpr (Python.Float value _ srcSpan) = return $ ConstFloat value srcSpan
convertExpr (Python.Imaginary value _ srcSpan) = return $ ConstImaginary value srcSpan
convertExpr (Python.Bool value srcSpan) = return $ ConstBool value srcSpan
convertExpr (Python.None srcSpan) = return $ ConstNone srcSpan
convertExpr (Python.ByteStrings values srcSpan) = return $ ConstString (foldl (++) "" $ map toRawString values) srcSpan
convertExpr (Python.Strings values srcSpan) = return $ ConstString (foldl (++) "" $ map toRawString values) srcSpan
convertExpr (Python.UnicodeStrings values srcSpan) = return $ ConstString (foldl (++) "" $ map toRawString values) srcSpan

convertExpr (Python.Var pyIdent srcSpan) = do
    lookupResult <- lookupVar $ Python.ident_string pyIdent
    case lookupResult of
        NotFound -> throwError "Variable not defined"
        prop -> return Prop {
                objProp = ObjPropIdentAnn (resultObjPropId prop) srcSpan,
                exprAnnot = SpanEmpty
            }

convertExpr (Python.Ellipsis _) = $notImplemented

convertExpr (Python.Call pyFun pyArgs srcSpan) = do
    funVar <- createUnnamedVarIdent
    funExprSegment <- stepWithBind funVar =<< convertExpr pyFun
    argsVar <- createUnnamedVarIdent
    mkArgs <- stepWithBind argsVar =<< parseLucretiaExprTemplate
        (Map.singleton "fun" funVar)
        Map.empty
        "module.__builtins__.__luc_mkArgs(fun)"
    let convertArg arg = case arg of
            Python.ArgExpr pyArgExpr _ -> do
                exprVar <- createUnnamedVarIdent
                exprSegment <- stepWithBind exprVar =<< convertExpr pyArgExpr
                argAppend <- parseLucretiaExprSegmentTemplate
                    (Map.fromList [("args", argsVar), ("expr", exprVar)])
                    Map.empty
                    "module.__builtins__.__luc_appendArg(args, expr)"
                return $ exprSegment . argAppend
            _ -> $notImplemented
    argSegments <- mapM convertArg pyArgs
    let fillingArgs = joinExprSegments argSegments
    applExpr <- parseLucretiaExprTemplate
        (Map.fromList [("fun", funVar), ("args", argsVar)])
        Map.empty $
        unlines [
                "let callable = module.__builtins__.__luc_getCallable(fun);",
                "callable.__luc_call(args)"
            ]
    let applExpr' = applExpr { exprAnnot = srcSpan }
    return $ funExprSegment . mkArgs . fillingArgs $ applExpr'

convertExpr (Python.Subscript _ _ _) = $notImplemented

convertExpr (Python.SlicedExpr _ _ _) = $notImplemented

-- http://docs.python.org/2/reference/expressions.html#conditional-expressions
convertExpr (Python.CondExpr pyTrueExpr pyCondExpr pyFalseExpr srcSpan) = do
    cond <- convertExpr pyCondExpr
    whenTrue <- convertExpr pyTrueExpr
    whenFalse <- convertExpr pyFalseExpr
    return IfElse {
            cond = cond,
            whenTrue = whenTrue,
            whenFalse = whenFalse,
            exprAnnot = srcSpan
        }

convertExpr (Python.BinaryOp pyOp pyLhs pyRhs srcSpan) = do
    lhs <- convertExpr pyLhs
    case pyOp of
        Python.And _ -> do
            lhsVar <- createUnnamedVarIdent
            rhs <- convertExpr pyRhs
            return LetIn {
                varId = buildVarId lhsVar,
                binding = lhs,
                body = IfElse {
                    cond = buildVar lhsVar,
                    whenTrue = rhs,
                    whenFalse = buildVar lhsVar,
                    exprAnnot = SpanEmpty
                },
                exprAnnot = srcSpan
            }
        Python.Or _ -> do
            lhsVar <- createUnnamedVarIdent
            rhs <- convertExpr pyRhs
            return LetIn {
                varId = buildVarId lhsVar,
                binding = lhs,
                body = IfElse {
                    cond = buildVar lhsVar,
                    whenTrue = buildVar lhsVar,
                    whenFalse = rhs,
                    exprAnnot = SpanEmpty
                },
                exprAnnot = srcSpan
            }
        _ ->
            case pyOp of
                Python.In _ -> $notImplemented
                Python.NotIn _ -> $notImplemented
                Python.Dot _ -> case pyRhs of
                    Python.Var pyPropId _ -> do
                        lhsVar <- createUnnamedVarIdent
                        lhsBind <- stepWithBind lhsVar lhs
                        let propName = Python.ident_string pyPropId
                        propId <- lookupOrCreatePropId propName
                        dotResolution <- parseLucretiaExprTemplate
                            (Map.singleton "lhs" lhsVar)
                            (Map.singleton "prop" propId) $
                            unlines [
                                    "let propChecker = func(obj) { ifhasattr (obj, prop) then True else False };",
                                    "let propGetter = func(obj) { obj.prop };",
                                    "let builtins = module.__builtins__;",
                                    "builtins.__luc_dot(lhs, propChecker, propGetter)"
                                ]
                        return $ lhsBind dotResolution
                    _ -> internalError "unexpected . operator rhs"
                _ -> do -- generic implementation for easy operators
                    op <- case pyOp of
                        Python.Exponent _ -> return Exp
                        Python.LessThan _ -> return Lesser
                        Python.GreaterThan _ -> return Greater
                        Python.Equality _ -> return Equal
                        Python.GreaterThanEquals _ -> return GreaterOrEq
                        Python.LessThanEquals _ -> return LesserOrEq
                        Python.NotEquals _ -> return NotEqual
                        Python.NotEqualsV2 _ -> return NotEqual
                        Python.Is _ -> return LocEqual
                        Python.IsNot _ -> return LocNotEqual
                        Python.BinaryOr _ -> return BitOr
                        Python.Xor _ -> return BitXor
                        Python.BinaryAnd _ -> return BitAnd
                        Python.ShiftLeft _ -> return ShiftLeft
                        Python.ShiftRight _ -> return ShiftRight
                        Python.Multiply _ -> return Mul
                        Python.Plus _ -> return Add
                        Python.Minus _ -> return Sub
                        Python.Divide _ -> return Div
                        Python.FloorDivide _ -> return FloorDiv
                        Python.Modulo _ -> return Mod
                        _ -> throwError "Invalid binary operator"
                    rhs <- convertExpr pyRhs
                    return $ Op op [lhs, rhs] srcSpan

convertExpr (Python.UnaryOp pyOp pyArg srcSpan) = do
    arg <- convertExpr pyArg
    op <- case pyOp of
        Python.Not _ -> return Not
        Python.Plus _ -> return Add
        Python.Minus _ -> return Sub
        Python.Invert _ -> return BitInv
        _ -> throwError "Invalid unary operator"
    return $ Op op [arg] srcSpan

convertExpr (Python.Lambda _ _ _) = $notImplemented

convertExpr (Python.Tuple _ _) = $notImplemented

convertExpr (Python.Yield _ _) = $notImplemented

convertExpr (Python.Generator _ _) = $notImplemented

convertExpr (Python.ListComp _ _) = $notImplemented
convertExpr (Python.List _ _) = $notImplemented

convertExpr (Python.Dictionary _ _) = $notImplemented
convertExpr (Python.DictComp _ _) = $notImplemented

convertExpr (Python.Set _ _) = $notImplemented
convertExpr (Python.SetComp _ _) = $notImplemented

convertExpr (Python.Starred _ _) = $notImplemented

convertExpr (Python.Paren expr _) = convertExpr expr

convertExpr (Python.StringConversion _ _) = $notImplemented

-- assign lhs expressions
-- allowed assign lhs expressions are listed here:
-- http://docs.python.org/2/reference/simple_stmts.html#assignment-statements
-- convertAssignLhsExpr returns expression segment to be executed before (possibly identity) and ObjPropIdent of actual rhs
convertAssignLhsExpr :: Python.ExprSpan -> VarEnv (ExprSpan -> ExprSpan, ObjPropIdentSpan)

convertAssignLhsExpr (Python.Var pyIdent srcSpan) = do
    lookupResult <- lookupOrCreateVar (Python.ident_string pyIdent)
    return (id, ObjPropIdentAnn lookupResult srcSpan)

convertAssignLhsExpr (Python.Subscript _ _ _) = $notImplemented

convertAssignLhsExpr (Python.SlicedExpr _ _ _) = $notImplemented

convertAssignLhsExpr (Python.BinaryOp (Python.Dot _) pyLhs pyRhs srcSpan) =
    case pyRhs of
        Python.Var pyPropId _ -> do
            lhs <- convertExpr pyLhs
            lhsVar <- createUnnamedVarIdent
            lhsBind <- stepWithBind lhsVar lhs
            let propName = Python.ident_string pyPropId
            propId <- lookupOrCreatePropId propName
            return (lhsBind, buildObjProp $ ObjPropIdent lhsVar propId)
        _ -> internalError "unexpected . operator rhs"

convertAssignLhsExpr (Python.Tuple _ _) = $notImplemented

convertAssignLhsExpr (Python.Yield _ _) = $notImplemented

convertAssignLhsExpr (Python.Generator _ _) = $notImplemented

convertAssignLhsExpr (Python.ListComp _ _) = $notImplemented
convertAssignLhsExpr (Python.List _ _) = $notImplemented

convertAssignLhsExpr (Python.Dictionary _ _) = $notImplemented
convertAssignLhsExpr (Python.DictComp _ _) = $notImplemented

convertAssignLhsExpr (Python.Set _ _) = $notImplemented
convertAssignLhsExpr (Python.SetComp _ _) = $notImplemented

convertAssignLhsExpr (Python.Starred _ _) = $notImplemented

convertAssignLhsExpr (Python.Paren expr _) = convertAssignLhsExpr expr

convertAssignLhsExpr _ = throwError "Not an lhs expression"
