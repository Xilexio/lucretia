module Language.Pretty.Value where

import Language.Lucretia.AST
import Data.Complex
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import qualified Data.Set as Set
import Language.Pretty.AST
import Language.Pretty.Common
import Language.Lucretia.RunEnvironment
import Language.Util.StringEscape
import Language.Util.VarMap (VarMap)
import qualified Language.Util.VarMap as VarMap

showValue :: RunEnvState -> Int -> VarMap String PropIdent -> Value -> String
showValue runEnvState maxDepth propIds val = case val of
    IntVal intVal -> show intVal
    BoolVal boolVal -> if boolVal then "True" else "False"
    FloatVal floatVal -> show floatVal
    ComplexVal complexVal -> show (realPart complexVal) ++ " + " ++ show (imagPart complexVal) ++ "j"
    StringVal stringVal -> "\"" ++ escapeString stringVal ++ "\""
    FunVal { funExpr = funExpr } -> "<" ++ show (length $ argVarIds funExpr) ++ "-arg function>"
    NoneVal -> "None"
    ObjectVal {} -> showValue' 0 val Set.empty where
        showValue' depth ObjectVal { location = loc } visitedObjectLocs =
            let objectSignature = "object(loc: " ++ show loc ++ ")"
                propertiesDesc = Map.foldlWithKey prettyShowProperty "" $ fromJust $ Map.lookup loc $ objects runEnvState where
                    updatedVisitedObjectLocs = Set.insert loc visitedObjectLocs
                    propIdDesc = prettyShow VarMap.empty propIds
                    prettyShowProperty str propId value =
                        str ++ showIndent (depth + 1) ++ propIdDesc propId ++ ": " ++ showValue' (depth + 1) value updatedVisitedObjectLocs ++ "\n" in
            if Set.member loc visitedObjectLocs then
                objectSignature ++ " { ... }"
            else if depth < maxDepth then
                objectSignature ++ " {" ++ (if propertiesDesc == "" then "" else "\n" ++ propertiesDesc) ++ showIndent depth ++ "}"
            else
                objectSignature
        showValue' indent val _ = showValue runEnvState maxDepth propIds val

showType :: Value -> String
showType (IntVal _) = "int"
showType (BoolVal _) = "bool"
showType (FloatVal _) = "float"
showType (ComplexVal _) = "complex"
showType (StringVal _) = "string"
showType (ObjectVal _) = "object"
showType (FunVal _ _) = "function"
showType (NoneVal) = "none"
