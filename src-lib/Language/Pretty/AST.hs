module Language.Pretty.AST where

import Language.Lucretia.AST
import Language.Lucretia.ASTFunctions (opToSign)
import Language.Python.Common.SrcLocation (SrcSpan (..))
import Language.Pretty.Common
import Language.Util.StringEscape
import qualified Language.Util.VarMap as VarMap
import Control.Lens
import Data.Maybe (fromMaybe)

instance PrettyShow (Expr annot) where
    prettyShow vm pm = prettyShow' 0 where
        prettyShowOther :: PrettyShow t => t -> String
        prettyShowOther = prettyShow vm pm
        prettyShow' :: Int -> Expr annot -> String
        prettyShow' indent expr = case expr of
            ConstInt { intValue = intValue } ->
                show intValue
            ConstBool { boolValue = boolValue } ->
                if boolValue then "True" else "False"
            ConstFloat { floatValue = floatValue } ->
                show floatValue
            ConstImaginary { imaginaryValue = imaginaryValue } ->
                show imaginaryValue ++ "j"
            ConstString { stringValue = stringValue } ->
                "\"" ++ escapeString stringValue ++ "\""
            ConstNone {} ->
                "None"
            Var { varId = varId } ->
                prettyShowOther varId
            Op { opType = opType, args = args } ->
                prettyShowOther opType ++ "(" ++ prettyShowList (prettyShow' $ indent + 1) args ++ ")"
            LetIn { varId = varId, binding = binding, body = body} ->
                "let " ++ prettyShowOther varId ++ " = " ++ prettyShow' (indent + 1) binding ++ " in\n" ++
                    showIndent indent ++ prettyShow' indent body
            New {} ->
                "new"
            Prop { objProp = objProp } ->
                prettyShowOther objProp
            AssignProp { objProp = objPropId, value = value } ->
                prettyShow vm pm objPropId ++ " = " ++ prettyShow' (indent + 1) value
            IfElse { cond = cond, whenTrue = whenTrue, whenFalse = whenFalse } ->
                "if " ++ prettyShow' (indent + 1) cond ++ " then\n" ++
                    showIndent (indent + 1) ++ prettyShow' (indent + 1) whenTrue ++ "\n" ++
                    showIndent indent ++ "else\n" ++
                    showIndent (indent + 1) ++ prettyShow' (indent + 1) whenFalse
            IfHasAttr { objExpr = objExpr, prop = prop, whenTrue = whenTrue, whenFalse = whenFalse } ->
                "ifhasattr(" ++ prettyShow' (indent + 1) objExpr ++ ", " ++ prettyShowOther prop ++ ") then\n" ++
                    showIndent (indent + 1) ++ prettyShow' (indent + 1) whenTrue ++ "\n" ++
                    showIndent indent ++ "else\n" ++
                    showIndent (indent + 1) ++ prettyShow' (indent + 1) whenFalse
            Fun { argVarIds = argVarIds, body = body } ->
                "func(" ++ prettyShowList prettyShowOther argVarIds ++ ") {\n" ++
                    showIndent (indent + 1) ++ prettyShow' (indent + 1) body ++ "\n" ++
                    showIndent indent ++ "}"
            Appl { fun = fun, args = args } ->
                let addParens = case fun of
                        Prop {} -> False
                        Var {} -> False
                        Fun {} -> False
                        _ -> True in
                (if addParens then "(" else "") ++ prettyShow' indent fun ++ (if addParens then ")" else "") ++
                    "(" ++ prettyShowList (prettyShow' $ indent + 1) args ++ ")"
            Import { filePath = filePath } ->
                "import(\"" ++ escapeString filePath ++ "\")"
        prettyShowList :: (t -> String) -> [t] -> String
        prettyShowList prettyShowFun elems = case elems of
            [] -> ""
            [e] -> prettyShowFun e
            e:es -> prettyShowFun e ++ ", " ++ prettyShowList prettyShowFun es

instance PrettyShow (VarIdentAnn annot) where
    prettyShow vm pm (VarIdentAnn rawVarId _) = prettyShow vm pm rawVarId

-- returns property name or default "v<id>" if it's internal or unknown
instance PrettyShow VarIdent where
    prettyShow vm _ varId =
        let defVarName = "v" ++ show (varIdNum varId) in
        fromMaybe defVarName (vm ^. VarMap.ids . at varId)

-- returns property name or default "p<id>" if it's internal or unknown
instance PrettyShow PropIdent where
    prettyShow _ pm propId =
        let defPropName = "p" ++ show (propIdNum propId) in
        fromMaybe defPropName (pm ^. VarMap.ids . at propId)

instance PrettyShow (ObjPropIdentAnn annot) where
    prettyShow vm pm objPropIdAnn = prettyShow vm pm $ rawObjProp objPropIdAnn

instance PrettyShow ObjPropIdent where
    prettyShow vm pm ObjPropIdent { objVarId = objVarId, objPropId = objPropId } =
        prettyShow vm pm objVarId ++ "." ++ prettyShow vm pm objPropId

instance PrettyShow OpType where
    prettyShow _ _ = opToSign

prettyShowFileExpr :: FileExprWithMetaData annot -> String
prettyShowFileExpr fileExpr = prettyShow VarMap.empty (fProps fileExpr) (fExpr fileExpr)

prettyShowSrcSpan :: SrcSpan -> String
prettyShowSrcSpan srcSpan = case srcSpan of
    SpanCoLinear {
            span_filename = filename,
            span_row = row,
            span_start_column = startCol,
            span_end_column = endCol
        } ->
            filename ++ ":" ++ show row ++ ":" ++ show startCol ++ "-" ++ show endCol
    SpanMultiLine {
            span_filename = filename,
            span_start_row = startRow,
            span_end_row = endRow
        } ->
            filename ++ ":" ++ show startRow ++ "-" ++ show endRow ++ ":*"
    SpanPoint {
            span_filename = filename,
            span_row = row,
            span_column = col
        } ->
            filename ++ ":" ++ show row ++ ":" ++ show col
    SpanEmpty -> "unknown"
