module Language.Pretty.Common where

import Language.Lucretia.AST
import Language.Util.Common (tabSize)
import Language.Util.VarMap (VarMap)

showIndent :: Int -> String
showIndent indent = replicate (tabSize * indent) ' '

class PrettyShow t where
    prettyShow :: VarMap String VarIdent -> VarMap String PropIdent -> t -> String
