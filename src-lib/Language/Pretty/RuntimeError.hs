module Language.Pretty.RuntimeError where

import Language.Lucretia.AST
import Language.Python.Common.SrcLocation (SrcSpan (..))
import Language.Pretty.AST
import Language.Pretty.Common
import Language.Lucretia.RuntimeError

instance PrettyShow RuntimeError where
    prettyShow varIds propIds err =
        let prettyShow' :: PrettyShow p => p -> String
            prettyShow' = prettyShow varIds propIds in
        prettyShowSrcSpan (srcSpan err) ++ ": " ++ case err of
            VarNotInScope varId _ ->
                "Variable `" ++ prettyShow' varId ++ "` not in scope"
            DotOnNonObject varId _ ->
                "Dot operator used on non-object variable `" ++ prettyShow' varId ++ "`"
            NonexistentPropReferenced ObjPropIdent { objVarId = varId, objPropId = propId } _ ->
                "Referenced nonexistent property `" ++ prettyShow' propId ++ "` of object `" ++ prettyShow' varId ++ "`"
            InvalidOpArgCount opType _ ->
                "Wrong number of arguments supplied to operator `" ++ prettyShow' opType ++ "`"
            InvalidOpArg opType _ ->
                "Object of invalid type supplied to operator `" ++ prettyShow' opType ++ "`"
            InvalidFunArgCount _ ->
                "Wrong number of arguments supplied to function"
            ApplOnNonFun _ ->
                "Application operator used on non-function"
            DivisionByZero opType _ ->
                "Division by zero in operator `" ++ prettyShow' opType ++ "`"
            NegativeShiftOffset opType _ ->
                "Negative shift offset in operator `" ++ prettyShow' opType ++ "`"
            ImportNotParsed filePath _ ->
                "Importing not parsed file `" ++ filePath ++ "`"

instance PrettyShow RuntimeErrorWithStackTrace where
    prettyShow varIds propIds (RuntimeErrorWithStackTrace err stackTrace') =
        let errMsg = prettyShow varIds propIds err
            stackTraceMsg = foldl (\acc srcSpan -> acc ++ "\n" ++ prettyShowSrcSpan srcSpan) "" stackTrace' in
        errMsg ++ "\nStacktrace:" ++ stackTraceMsg
