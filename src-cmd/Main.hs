module Main (
    main
) where

import Control.Monad.Except (lift, runExceptT)
import Options (runCommand)
import CmdLineOptions (convertMode, interactiveMode, interpretedMode, preprocessCommand, pythonMode)
import Language.Lucretia.FileInterpreter (lucretiaFileInterpreter)
import Language.Lucretia.InteractiveInterpreter (lucretiaInteractiveInterpreter)
import Language.Python.FileConverter (pythonFileConverter)
import Language.Python.FileInterpreter (pythonFileInterpreter)
import System.Exit (exitFailure, exitSuccess)
import System.IO (hPutStrLn, stderr)


main :: IO ()
main = do
    err <- runExceptT . runCommand . preprocessCommand $ \opts args -> if
        | interactiveMode opts -> lift lucretiaInteractiveInterpreter
        | interpretedMode opts -> lift . lucretiaFileInterpreter $ head args
        | convertMode opts -> lift . pythonFileConverter $ head args
        | pythonMode opts -> lift . pythonFileInterpreter $ head args
    case err of
        Right _ -> exitSuccess
        Left errMsg -> do
            hPutStrLn stderr errMsg
            exitFailure