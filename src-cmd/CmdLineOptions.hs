module CmdLineOptions (
    ProgramOptions (..),
    preprocessCommand
) where

import Control.Monad.Except
import Control.Applicative (pure, (<*>))
import Data.List (isSuffixOf, intercalate)
import Options
import Safe

data ProgramOptions = ProgramOptions {
        interactiveMode :: Bool,
        interpretedMode :: Bool,
        convertMode :: Bool,
        pythonMode :: Bool
    }

modeGroup = Just $ group "mode" "Program mode"
    "Mode in which program runs. Only a single mode may be specified. By default it runs in interactive, \
    \interpreted or Python mode depending on arguments."

instance Options ProgramOptions where
    defineOptions = pure ProgramOptions
        <*> defineOption optionType_bool (\o -> o {
                optionShortFlags = "a",
                optionLongFlags = ["interactive"],
                optionDescription =
                    "Runs Lucretia interactive interpreter. Expects no files.",
                optionGroup = modeGroup
            })
        <*> defineOption optionType_bool (\o -> o {
                optionShortFlags = "i",
                optionLongFlags = ["interpreted"],
                optionDescription =
                    "Runs Lucretia file interpreter. Expects one file.",
                optionGroup = modeGroup
            })
        <*> defineOption optionType_bool (\o -> o {
                optionShortFlags = "c",
                optionLongFlags = ["convert"],
                optionDescription =
                    "Runs Python file to Lucretia converter. Expects one file.",
                optionGroup = modeGroup
            })
        <*> defineOption optionType_bool (\o -> o {
                optionShortFlags = "p",
                optionLongFlags = ["python"],
                optionDescription =
                    "Runs Python file interpreter through conversion to Lucretia and prints top-level \"test\" variable. Expects one file.",
                optionGroup = modeGroup
            })

-- program modes and their expected number of arguments
programModes = [(interactiveMode, 0), (interpretedMode, 1), (convertMode, 1), (pythonMode, 1)]

preprocessCommand program opts args = do
    let setModes o = filter (\(mode, _) -> mode o) programModes
    opts' <- case setModes opts of
        [] -> case args of
            [] -> return opts { interactiveMode = True }
            [filePath] ->
                if ".py" `isSuffixOf` filePath then
                    return opts { pythonMode = True }
                else
                    return opts { interpretedMode = True }
            _ ->
                throwError "Automatic program mode choosing supports only up to one argument."
        [_] -> return opts
        _ -> throwError "Only one program mode flag can be supplied at once."
    case setModes opts' of
        [(_, argsCount)] ->
            unless (argsCount == length args) $ throwError $
                "Wrong number of supplied arguments (expected " ++ show argsCount ++ ", got " ++ show (length args) ++ ")"
        _ -> error "Internal error: a single program mode should have been chosen."
    program opts' args
