#!/bin/bash
COUNT=$1

echo "def fun(i):"

for ((i = 0; i < COUNT; ++i)); do

INDENT=`for ((j = 0; j < i; ++j)); do echo -n "  "; done`

echo " ${INDENT}while i > $i:"
echo " ${INDENT} if i > $((i / 2)):"
echo " ${INDENT}  i = i - 2"
echo " ${INDENT}  continue"
echo " ${INDENT} else:"
echo " ${INDENT}  i = i - 1"
echo " ${INDENT}  if i == 12:"
echo " ${INDENT}   return 42"

done

echo "fun(42424242)"
