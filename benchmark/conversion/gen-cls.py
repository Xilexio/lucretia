#!/bin/bash
DEPTH=$1

echo "test = 0"

echo "class A0:"
echo "  def foo(self):"
echo "    return 1"

for ((i = 1; i <= DEPTH; ++i)); do
  echo "class A$i(A$((i-1))):"
  echo "  pass"
  echo "obj$i = A$i()"
  echo "test = test + obj$i.foo()"
done
