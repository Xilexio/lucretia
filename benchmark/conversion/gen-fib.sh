#!/bin/bash
COUNT=$1

for ((i = 0; i < COUNT; ++i)); do
  echo "def fib$i():"
  echo "  a = 0"
  echo "  b = 1"

  for ((j = 0; j < i; ++j)); do
  echo "  t = b"
  echo "  b = a + b"
  echo "  a = t"
  done

  echo "  return a"
done
