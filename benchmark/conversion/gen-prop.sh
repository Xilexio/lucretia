#!/bin/bash
COUNT=$1

echo "class X: pass"
echo "x = X()"
echo "x.p = x"
echo "x.v = 42"
echo -n "test = x"

for ((i = 0; i < COUNT; ++i)); do
echo -n ".p"
done

echo ".v"
