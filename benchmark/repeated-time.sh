#!/bin/bash
N=$1
shift 1
echo "#!/bin/bash" > .run
for ((i = 0; i < N; ++i)); do
  echo $@ >> .run
done
chmod +x .run
time .run
rm .run
