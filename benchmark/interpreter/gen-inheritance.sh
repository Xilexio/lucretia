#!/bin/bash
DEPTH=1000

echo "class A0:"
echo "  def foo(self):"
echo "    return 1"

for ((i = 1; i <= DEPTH; ++i)); do
  echo "class A$i(A$((i-1))):"
  echo "  pass"
done

echo "obj = A$DEPTH()"
echo "test = 0"

echo "i = 0"
echo "while i < 100:"
echo "  test = test + obj.foo()"
echo "  i = i + 1"
