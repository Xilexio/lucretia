class Counter:
  def __init__(self):
    self.cnt = 0

  def inc(self):
    self.cnt = self.cnt + 1

  def count(self):
    return self.cnt

c = Counter()
i = 0
while i < 50000:
  c.inc()
  i = i + 1
test = c.count()
