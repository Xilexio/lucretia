def fib(n):
  a = 0
  b = 1
  while n != 0:
    t = b
    b = a + b
    a = t
    n = n - 1
  return a

test = fib(50000)
